# README #

### What is this repository for? ###

Symbol Recognition

### tested system ###
- Debian GNU/Linux 8 (jessie) 64-Bit
- Google Chrome 51.0.2704.106 
- mysql  Ver 14.14 Distrib 5.5.49
- readline 6.3
- eclipse Neon Release (4.6.0)
- java version 1.8.0_91
- jetty jetty-9.3.3.v20150827


### How do I get set up? ###
- requires mysql server, jetty version >= 9, java >= 8, maven, uses maven-jetty-plugin

- clone git-repository:
```
#!bash

git clone https://rapit@bitbucket.org/rapit/bachelor.git
```

- eclipse install maven project via git import

- import sql database "symbols" (src/resources) on local mysql server as mysql://localhost:3306/symbols):


```
#!bash

mysql -u user -p psswd symbols < symbols/src/main/resources/symbols.sql

```
- place weka config:
```
#!bash

cp symbols/src/main/resources/DatabaseUtils.props ~/wekafiles/props/DatabaseUtils.props
```
- run via maven build: clean compile exec:java -e  (set base direcotry to symbols)
- classifier on http://localhost:8080/index.html

### Classification Mode ###
- use classify Switch for clsasification mode
- training data is stored in local database
- export via 
```
#!bash

mysqldump -u user -p psswd --skip-extended-insert symbols > symbols/src/main/resources/symbols.sql
```
### Update Database ###
Update and refresh existing database via update-button. Recursively parses symbol-files from src/main/resources/raw. Every single symbol needs one .svg and one .png file for successful parsing.

### Use boogie-board as input device  ###

- [https://github.com/jbedo/boogiesync-tablet](https://github.com/jbedo/boogiesync-tablet)
-required python 2.7
- pip2.7 install pyusb
- pip2.7 install evdev (downgrade may be required..)
- run via python2.7 usb-driver.py