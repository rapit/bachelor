var self = this;
var paint = false;
var canvas = document.getElementById('canvasElement');
var mapEl = document.getElementById('map');

var context = canvas.getContext("2d");
var symbolPoints = new Array();
var startEndPoints = new Array();
var curvaturePoints;
var featurePoints = new Array();

var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var paint;
var idRandom = Math.round(Math.random(1024) * 1000);

var JSONdata; // last element contains information about specific request
var gridData;
var pointData;

var globalGridFactor = 20;

var compareMode = 1;
// 0: distance
// 1: point-compare
// 2: weka (default)
// 3: weka-grid
// 4: weka-ranker
// -1: Training mode

var classificator = -1;
// default
var trainingWeight = 1;

var activePin = false;
var mapDefaultLong = 8.420066;
var mapDefaultLat = 49.010570;
var mapDefaultZoom = 18;
var selectedRow;
var geojsonObject;
var features = null;
var updateState = false;

var HttpClient = function() {
	this.get = function(aUrl, aCallback) {
		var anHttpRequest = new XMLHttpRequest();
		anHttpRequest.onreadystatechange = function() {
			if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
				aCallback(anHttpRequest.responseText);
		}

		anHttpRequest.open("GET", aUrl, true);
		anHttpRequest.send(null);
	}

	this.post = function(aUrl, params, aCallback) {
		var anHttpRequest = new XMLHttpRequest();
		anHttpRequest.onreadystatechange = function() {
			if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
				aCallback(anHttpRequest.responseText);
		}

		anHttpRequest.open("POST", aUrl, true);
		anHttpRequest.setRequestHeader('Content-Type',
				'application/x-www-form-urlencoded');

		anHttpRequest.send(params);
	}
}

var client = new HttpClient();

var source = new ol.source.XYZ({
	url : 'http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png'
});
var icons = [];

var map = new ol.Map({
	layers : [ new ol.layer.Tile({
		source : source
	}), new ol.layer.Vector({
		source : new ol.source.Vector({
			features : icons
		})
	}) ],
	interactions : ol.interaction.defaults({
		mouseWheelZoom : false
	}),

	controls : [],
	target : 'map',
	view : new ol.View({
		center : ol.proj.transform([ mapDefaultLong, mapDefaultLat ],
				'EPSG:4326', 'EPSG:3857'),
		zoom : mapDefaultZoom
	})
});

var external_zoomControl = new ol.control.Zoom({
	target : document.getElementById('externalZoomControl')
});
map.addControl(external_zoomControl);

// var select = new ol.interaction.Select({
// //some options
// });
// map.addInteraction(select);
//
// var selected_collection = select.getFeatures();
//
// map.addInteraction(select);

// var bbox = [];
// var viewPort = map.getView().calculateExtent(map.getSize());
// bbox[0] = ol.proj.transform([ viewPort[0], viewPort[1] ], 'EPSG:3857',
// 'EPSG:4326');
// bbox[1] = ol.proj.transform([ viewPort[2], viewPort[3] ], 'EPSG:3857',
// 'EPSG:4326');

// client.get('http://www.overpass-api.de/api/xapi_meta?*[building=yes][bbox='
// + bbox[0][0] + ',' + bbox[0][1] + ',' + bbox[1][0] + ',' + bbox[1][1]
// + ']', function(response) {
// console.log(response);
// });

map.on('moveend', function() {
	updateMap();
});

map.on('zoomend', function() {
	updateMap();
});

var lastTouch;
function getTouchPos(canvasDom, touchEvent) {
	  var rect = canvasDom.getBoundingClientRect();
	  return {
	    x: touchEvent.touches[0].clientX - rect.left,
	    y: touchEvent.touches[0].clientY - rect.top
	  };
}

window.onload = function() {
	// canvas.style.pointerEvents = "none";

	document.getElementById('floatingBarsG').style.display = "none";

	document.getElementById('latPicker').value = mapDefaultLat;
	document.getElementById('longPicker').value = mapDefaultLong;

	document.addEventListener("keydown", function(e) {
		if (e.ctrlKey || e.keyCode == 18) {
			activePin = !activePin;

			if (activePin) {
				canvas.style.pointerEvents = "none";
				canvas.style.border = "3px solid blue";

			} else {
				canvas.style.pointerEvents = "all";

				if (updateState) {
					canvas.style.border = "3px solid red";

				} else {
					canvas.style.border = "1px solid black";
				}


			}

		}

	});
	
	canvas.addEventListener("touchstart", function (e) {
//      mousePos = getTouchPos(canvas, e);
      var touch = e.touches[0];
      lastTouch = touch;
		var rect = canvas.getBoundingClientRect();

      var mouseEvent = new MouseEvent("mousedown", {
      	clientX: touch.clientX + window.scrollX,
      	clientY: touch.clientY + window.scrollY
      });
      canvas.dispatchEvent(mouseEvent);
		console.log("touch start");
		e.preventDefault();

//		var rect = canvas.getBoundingClientRect();
//		var mouseX = e.pageX - rect.left - window.scrollX;
//		var mouseY = e.pageY - rect.top - window.scrollY;
//
//		paint = true;
//		addPos(mouseX, mouseY, false);
//		addStartEndPoint(mouseX, mouseY);
//		draw();

	}, false);
	
	canvas.addEventListener("touchend", function (e) {
		var touch = lastTouch;
		var rect = canvas.getBoundingClientRect();

		if (touch) {
				var mouseEvent = new MouseEvent("mouseup", {
		        	clientX: touch.clientX + window.scrollX,
		        	clientY: touch.clientY + window.scrollY
	        });
		} else {
			var mouseEvent = new MouseEvent("mouseup", {});
		}

		canvas.dispatchEvent(mouseEvent);
		e.preventDefault();

		console.log("touch end");
	}, false);
	
	
	canvas.addEventListener("touchmove", function (e) {
		console.log("touch event fired");
		var touch = e.touches[0];
		lastTouch = touch;
		var rect = canvas.getBoundingClientRect();

			var mouseEvent = new MouseEvent("mousemove", {
	        	clientX: touch.clientX + window.scrollX,
	        	clientY: touch.clientY + window.scrollY
		});
		canvas.dispatchEvent(mouseEvent);
		e.preventDefault();
	}, false);

	canvas.addEventListener("mousedown", function(e) {

		if (e.which == 1 && !activePin) {
			var rect = canvas.getBoundingClientRect();
			var mouseX = e.pageX - rect.left - window.scrollX;
			var mouseY = e.pageY - rect.top - window.scrollY;

			paint = true;
			addPos(mouseX, mouseY, false);
			addStartEndPoint(mouseX, mouseY);
			draw();
		} else if (e.which == 2) {
			e.preventDefault();
			clearCanvas();
		}

	});

	canvas.addEventListener("contextmenu", function(e) {
		e.preventDefault();
		return false;
	});
	mapEl.addEventListener("contextmenu", function(e) {
		e.preventDefault();
		return false;
	});

	canvas.addEventListener("mousemove", function(e) {
		if (paint) {
			var rect = canvas.getBoundingClientRect();
			var mouseX = e.pageX - rect.left - window.scrollX;
			var mouseY = e.pageY - rect.top - window.scrollY;
			addPos(mouseX, mouseY, true);
			draw();
		}
	});

	canvas.addEventListener("mouseup", function(e) {
		if (paint) {
			var rect = canvas.getBoundingClientRect();
			var mouseX = e.pageX - rect.left - window.scrollX;
			var mouseY = e.pageY - rect.top - window.scrollY;
			addStartEndPoint(mouseX, mouseY);
			newPaint();

		}
		paint = false;
		canvas.style.pointerEvents = "all";

	});

	canvas.addEventListener("mouseleave", function(e) {
		if (paint) {

			var rect = canvas.getBoundingClientRect();
			var mouseX = e.pageX - rect.left - window.scrollX;
			var mouseY = e.pageY - rect.top - window.scrollY;
			addStartEndPoint(mouseX, mouseY);
			newPaint();
		}
		paint = false;

	});

	function addStartEndPoint(x, y) {
		this.startEndPoints.push([ x, y ]);
	}

	function addPos(x, y, dragging) {
		clickX.push(x);
		clickY.push(y);
		clickDrag.push(dragging);
	}

	function draw() {
		context.clearRect(0, 0, context.canvas.width, context.canvas.height);

		context.strokeStyle = "#000000";
		context.lineJoin = "round";
		context.lineWidth = 5;
		this.symbolPoints = [];

		for (var i = 0; i < clickX.length; i++) {
			context.beginPath();
			if (clickDrag[i] && i) {
				context.moveTo(clickX[i - 1], clickY[i - 1]);
			} else {
				context.moveTo(clickX[i] - 1, clickY[i]);
			}
			context.lineTo(clickX[i], clickY[i]);
			this.symbolPoints.push([ clickX[i], clickY[i] ]);
			context.closePath();
			context.stroke();
		}

	}

}

function SelectRow(row) {
	if (selectedRow !== undefined) {
		selectedRow.style.background = "white";
	}
	// selected_collection.clear();

	if (selectedRow == row) {
		row.style.background = "white";
		selectedRow = undefined;
		showOneId()
	} else {
		showOneId(row.cells[2].innerHTML);
		selectedRow = row;
		selectedRow.style.background = "#CBCBE2";
	}

}

function addEvent(element, evt, callback) {
	if (element.addEventListener) {
		element.addEventListener(evt, callback, false);
	} else if (element.attachEvent) {
		element.attachEvent("on" + evt, callback);
	} else {
		element["on" + evt] = callback;
	}
}

function updateMap() {
	
	var douglasPeucker = document.getElementById('distanceTolerance').value;	

	updateState = true;
	if (!activePin) {
		canvas.style.border = "3px solid red";
	}
	var zoom = map.getView().getZoom();
	var xy = ol.proj.transform([ map.getView().getCenter()[0],
			map.getView().getCenter()[1] ], 'EPSG:3857', 'EPSG:4326');
	console.log(long2tile(xy[0], zoom) + '/' + lat2tile(xy[1], zoom));
	client.get('http://vector.mapzen.com/osm/buildings/' + zoom + '/'
			+ long2tile(xy[0], zoom) + '/' + lat2tile(xy[1], zoom)
			+ '.json?api_key=vector-tiles-oxmyuks', function(response) {
		addVectorSource(response);
		client.post('update' + "?" + "douglasPeucker=" + douglasPeucker , response, function(response2) {
			console.log(response2);
			updateState = false;
			if (!activePin) {
				canvas.style.border = "1px solid black";

			}
		});
	});
	


}

function addVectorSource(response) {
	features = new ol.format.GeoJSON().readFeatures(response, {
		featureProjection : 'EPSG:3857'
	});
	var vectorSource = new ol.source.Vector({
		features : features
	});

	var vectorLayer = new ol.layer.Vector({
		source : vectorSource,
		style : null
	});

	map.addLayer(vectorLayer);

}

function showOneId(id) {
	var i, singlefeature;

	if (!features) {
		return;
	}
	var style = new ol.style.Style({
		stroke : new ol.style.Stroke({
			color : 'blue',
			lineDash : [ 4 ],
			width : 3
		}),
		fill : new ol.style.Fill({
			color : 'rgba(0, 0, 255, 0.1)'
		})
	});

	var len = features.length;
	if (id !== undefined) {
		for (i = 0; i < len; i++) {
			singlefeature = features[i];
			singlefeature.setStyle(null);

			// Resetting feature style back to default function given in
			// defaultPointStyleFunction()
			if (singlefeature.get('id') == id) {
				singlefeature.setStyle(style);
			}
		}
	}
	// No id was provided - all points are hidden
	else {
		for (i = 0; i < len; i++) {
			singlefeature = features[i];
			singlefeature.setStyle(null);
		}
	}

}

function long2tile(lon, zoom) {
	return (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
}
function lat2tile(lat, zoom) {
	return (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1
			/ Math.cos(lat * Math.PI / 180))
			/ Math.PI)
			/ 2 * Math.pow(2, zoom)));
}

function changeLatLong() {
	var lat = parseFloat(document.getElementById('latPicker').value);
	var lon = parseFloat(document.getElementById('longPicker').value);
	console.log(lat + "|" + lon);
	console.log(ol.proj.transform(map.getView().getCenter(), 'EPSG:3857',
			'EPSG:4326'));
	if ((lat > -90 && lat < 90) && (lon > -180 && lon < 180)) {
		map.getView().setCenter(
				ol.proj.transform([ lon, lat ], 'EPSG:4326', 'EPSG:3857'),
				map.getView().getZoom());
	}
}

function getValue(id) {
	var element = document.getElementById(id);
	var value = element.value;
	if (!value)
		return;
	return value;
}

function newPaint() {
	resetCurvatureValues(getValue("dminNumber"), getValue("dmaxNumber"),
			getValue("amaxNumber"));
	var curvatureSum = this.curvaturePoints.length;
	this.curvaturePoints = calculateCurvaturePoints(this.symbolPoints);

	var checked = document.getElementById('checkboxCurvature').checked;

	if (this.startEndPoints.length >= 2) {
		this.featurePoints
				.push(this.startEndPoints[this.startEndPoints.length - 2]);
		for (var i = curvatureSum; i < this.curvaturePoints.length; i++) {
			this.featurePoints.push(this.symbolPoints[curvaturePoints[i]]);
		}
		this.featurePoints
				.push(this.startEndPoints[this.startEndPoints.length - 1]);
	}
	if (checked) {

		// console.log(this.symbolPoints);
		for (var i = 0; i < this.curvaturePoints.length; i++) {
			var point = this.symbolPoints[this.curvaturePoints[i]];
			this.context.strokeStyle = "#FF0000";
			this.context.beginPath();
			this.context.moveTo(point[0], point[1]);
			this.context.arc(point[0], point[1], 5, 0, 2 * Math.PI, false);
			this.context.stroke();

		}

		for (var i = 0; i < this.startEndPoints.length; i++) {
			var point = this.startEndPoints[i];
			this.context.strokeStyle = "#00FF00";
			this.context.beginPath();
			this.context.moveTo(point[0], point[1]);
			this.context.arc(point[0], point[1], 5, 0, 2 * Math.PI, false);
			this.context.stroke();
		}
	}

	self.gridData = getGrid();
	deleteTable();
	idRandom = Math.round(Math.random(1024) * 1000);
	updateSugg(self.gridData.data, canvas.width / self.gridData.factor);

}

function updatePreview(previewCompare) {

	switch (compareMode) {
	case 0:
		if (self.gridData != null) {
			createHammingPreview(self.gridData.data, self.gridData.factor,
					previewCompare);
		}
		break;
	case 1:
		createRotadedPointComparePreview(self.pointData, context.canvas.width,
				context.canvas.height, previewCompare)
		// console.log("created Point data with height: " +
		// context.canvas.height);
		break;

	default:
		clearPreview();
		break;
	}

}

function deleteTable() {
	var new_tbody = document.createElement('tbody');
	var old_tbody = document.getElementById('resulttable')
			.getElementsByTagName('tbody')[0];
	old_tbody.parentNode.replaceChild(new_tbody, old_tbody)
}

function updateSugg(gridData, gridSize) {

	document.getElementById('floatingBarsG').style.display = "block";
	var checkBoxRotation = document.getElementById('checkBoxRotationInvariant').checked;
	var rotationInvariance = 0;
	if (checkBoxRotation) {
		rotationInvariance = 1;
	}
	
	var listNearPointFilter = document.getElementById('nearPointFilterSelect').value;	
	
	// console.log(self.featurePoints);
	var hammingString = "";
	for (var i = 0; i < gridData.length; ++i) {
		if (gridData[i]) {
			hammingString = hammingString + "1";
		} else {
			hammingString = hammingString + "0";

		}
	}

	var tableRef = document.getElementById('resulttable').getElementsByTagName(
			'tbody')[0];
	// var sendCurvaturePoints = new Array();
	//	
	// for (var i=0; i < this.curvaturePoints.length; i++) {
	// sendCurvaturePoints.push(this.symbolPoints[this.curvaturePoints[i]]);
	//		
	// }

	var dataURL = {
		"data" : canvas.toDataURL(),
		"hamming" : hammingString,
		"size" : gridSize,
		"startendPoints" : JSON.stringify(this.startEndPoints),
		"featurePoints" : JSON.stringify(this.featurePoints),
		"method" : self.compareMode,
		"length" : self.symbolPoints.length,
		"classificator" : self.classificator,
		"mode" : 1,
		"rotationInvariance" : rotationInvariance,
		"nearPointFilter" : listNearPointFilter
	};

	// console.log("testpoints");
	// console.log(sendCurvaturePoints);
	// var data = context.getImageData(0, 0, canvas.width, canvas.height);
	var jsonData = JSON.stringify(dataURL);
	// console.log(jsonData);
	client.post('classify', jsonData, function(response) {
		// console.log(response);
		var data = JSON.parse(response);
		self.JSONdata = data;
		for (var i = 0; i < data.length - 1; i++) {
			(function(obj) {
				var geoIdText = document.createTextNode(obj.geoId);
				var idText = document.createTextNode(obj.addrStreet + " "
						+ obj.housenumber);
				var probText = document.createTextNode(obj.prob.toFixed(2));

				var rankText = document.createTextNode(i + 1);
				var image = document.createElement("img");
				image.setAttribute("id", "rankingImage" + (i));
				image.setAttribute('alt', 'na');
				image.setAttribute('height', '25');
				image.setAttribute('width', 'auto');

				var arrayBuffer = obj.image;
				var bytes = new Uint8Array(arrayBuffer);
				var blob = new Blob([ bytes.buffer ]);

				var reader = new FileReader();
				reader.onload = function(e) {
					image.src = e.target.result;
					// console.log(image.src);
				};
				reader.readAsDataURL(blob);

				var newRow = tableRef.insertRow(tableRef.rows.length);

				var newCell1 = newRow.insertCell(-1);
				newCell1.appendChild(rankText);

				var newCell2 = newRow.insertCell(-1);
				newCell2.appendChild(probText);

				var newCell3 = newRow.insertCell(-1);
				newCell3.appendChild(geoIdText);
				var newCell4 = newRow.insertCell(-1);
				newCell4.appendChild(idText);
				var newCell5 = newRow.insertCell(-1);
				newCell5.appendChild(image);

			})(data[i]);

		}
		var rows = tableRef.getElementsByTagName("tr");
		for (var i = 0; i < rows.length; i++) {
			(function(idx) {
				addEvent(rows[idx], "click", function() {
					SelectRow(rows[idx]);
				});
			})(i);
		}
		document.getElementById('floatingBarsG').style.display = "none";
		if (this.compareMode == -1) {
			updateInfoBar(data[data.length - 1]);
		} else {
			resetInfoBar();
		}
		updatePreview(-1);

	});
}

function clearCanvas() {
	context.clearRect(0, 0, context.canvas.width, context.canvas.height);
	var preCanvas = document.getElementById('previewCanvas');
	var preContext = preCanvas.getContext("2d");
	preContext.clearRect(0, 0, preContext.canvas.width,
			preContext.canvas.height);

	clickX = [];
	clickY = [];
	clickDrag = [];
	startEndPoints = [];
	curvaturePoints = [];
	featurePoints = [];
	deleteTable();
	resetInfoBar();
	self.gridData = null;

}

function savePNG() {
	var pngbtn = document.getElementById('pngbtn');
	pngbtn.href = canvas.toDataURL();
	pngbtn.download = idRandom;
}
function saveSVG() {
	var svgbtn = document.getElementById('svgbtn');
	svgbtn.href = canvas.toDataURL();
	svgbtn.download = idRandom;
}

function intitializeDatabase() {
	client.get('update' + "?" + "grid=" + (canvas.width / globalGridFactor),
			function(response) {
				// console.log("updated Database");

			});
}

function comparePreview() {
	var checked = document.getElementById('checkboxComparePreview').checked;
	if (checked) {
		if (self.JSONdata != null) {
			var previewRef = document.getElementById('buttonGroupPreview');
			var length = self.JSONdata.length - 1;
			// console.log("json not null");
			for (var i = 0; i < length; i++) {
				// console.log("add button");
				var btn = document.createElement('button');
				btn.setAttribute("id", "previewButton" + (i + 1));
				btn.setAttribute("type", "button");
				btn.setAttribute("class", "btn btn-xs btn-default")
				btn.setAttribute("style", "text-align:center;");
				btn.setAttribute("onClick",
						"comparePreviewButtonClicked(this.innerHTML)");
				btn.innerHTML = (i + 1);

				previewRef.appendChild(btn);
				if (i == 0) {
					btn.setAttribute("autofocus", "true");
					updatePreview(0);

				}
			}

		}
	} else {
		var previewRef = document.getElementById('buttonGroupPreview');
		while (previewRef.firstChild) {
			previewRef.removeChild(previewRef.firstChild);
		}
		updatePreview(-1);
	}
}

function cutPrefix(s, prefix) {
	var length = prefix.length;
	return s.substring(length);
}

function comparePreviewButtonClicked(id) {
	updatePreview(id - 1);
}

function downloadTextFile(filename, text) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,'
			+ encodeURIComponent(text));
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

function updateRankingOrder() {
	var resultTableBody = document.getElementById('resulttable')
			.getElementsByTagName('tbody')[0];
	var rows = resultTableBody.getElementsByTagName('tr');
	var firstRow = rows[0];
	var lastRow = rows[rows.length];
	firstRow.parentNode.insertBefore(lastRow.parentNode.removeChild(lastRow),
			firstRow);

}

function changeMethod() {
	var selected_option = document.getElementById('algorithmSelect').selectedIndex;
	if (selected_option >= 0) {
		self.compareMode = selected_option;
		deleteTable();
		if (self.gridData != null) {
			updateSugg(self.gridData.data, canvas.width / self.gridData.factor);
		}

	}

}

function show_hide_column(col_no, do_show) {

	var stl;
	if (do_show)
		stl = 'table-cell'
	else
		stl = 'none';

	var tbl = document.getElementById('resulttable');
	var rows = tbl.getElementsByTagName('tr');

	for (var row = 0; row < rows.length; row++) {

		var cels
		if (row == 0) {
			cels = rows[row].getElementsByTagName('th')

		} else {
			cels = rows[row].getElementsByTagName('td')

		}
		cels[col_no].style.display = stl;
	}
}
function switchTrainingMode() {
	var selected_option = document.getElementById('trainingModeSwitch').checked;
	if (selected_option) {
		self.compareMode = -1;
		deleteTable();
		if (self.gridData != null) {
			updateSugg(self.gridData.data, canvas.width / self.gridData.factor);
		}
		// console.log(document.getElementById('settingsBar'));
		document.getElementById('settingsBar').style.display = "none";
		document.getElementById('processingbar').style.display = "none";
		document.getElementById('wekabar').style.display = "none";
		document.getElementById('previewingbar').style.display = "none";
		show_hide_column(0, false);
		show_hide_column(1, false);
		var tableArea = document.getElementById('areaNextToTable');
		tableArea.setAttribute("class", "col-md-3");
		var informationBar = document.getElementById('infobarCol');
		informationBar.setAttribute("class", "col-md-3");
		var row = document.getElementById('row1');
		row.appendChild(informationBar);
		var title = document.getElementById('title');
		var canvasBorder = document.getElementById('canvasElement');
		canvasBorder.style.border = "1px solid blue";
		var weightButtons = document.getElementById('weightButtons');
		weightButtons.style.display = "inline";
		resetWeightButtons(1);

	} else {
		document.getElementById('weightButtons').style.display = "none";

		document.getElementById('settingsBar').style.display = "block";
		document.getElementById('processingbar').style.display = "block";
		document.getElementById('wekabar').style.display = "block";
		document.getElementById('previewingbar').style.display = "block";
		var tableArea = document.getElementById('areaNextToTable');
		tableArea.setAttribute("class", "col-md-5 col-md-offset-1");
		var informationBar = document.getElementById('infobarCol');
		informationBar.setAttribute("class", "col-md-4 topmargin");
		var row2 = document.getElementById('row2');
		row2.insertBefore(informationBar, row2.firstChild);
		var canvasBorder = document.getElementById('canvasElement');
		canvasBorder.style.border = "1px solid black";

		show_hide_column(0, true);
		show_hide_column(1, true);
		changeMethod();
	}

}



