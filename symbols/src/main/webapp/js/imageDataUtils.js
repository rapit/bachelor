function resizeImageData(canvas, symbolPoints, size) {
	var minX = size, minY = size, maxX = 0.0, maxY = 0.0;
	for (var i = 0; i < symbolPoints.length; i++) {
		if (symbolPoints[i][0] < minX) {
			minX = symbolPoints[i][0];
		}
		if (symbolPoints[i][1] < minY) {
			minY = symbolPoints[i][1];
		}
		if (symbolPoints[i][0] > maxX) {
			maxX = symbolPoints[i][0];
		}
		if (symbolPoints[i][1] > maxY) {
			maxY = symbolPoints[i][1];
		}
	}
	var dX = maxX - minX;
	var dY = maxY - minY;
	var scaleFactor = 1.0;
	var xOffset  = 0.0, yOffset = 0.0;
	
	if (dX > dY) {
		if (dX == 0) {
			scaleFactor = 1;
		} else {
			scaleFactor = size / dX;

		}
		yOffset = (size - (dY * scaleFactor)) / 2;

	} else {
		if (dY == 0) {
			scaleFactor = 1;
		} else {
			scaleFactor = size / dY;

		}
		xOffset = (size - (dX * scaleFactor)) / 2;

	}
	xOffset = Math.floor(xOffset);
	yOffset = Math.floor(yOffset);
	minX = Math.ceil(minX);
	minY = Math.ceil(minY);
	
	
	var newCanvas1 = document.createElement('canvas');
	newCanvas1.height = size;
	newCanvas1.width = size;
	newContext1 = newCanvas1.getContext("2d");
	newContext1.drawImage(canvas,minX,minY, dX, dY,xOffset,yOffset,size - (2* xOffset), size - (2*yOffset));	

	return newContext1.getImageData(0, 0, size, size);
	
	
}/**
	 * 
	 */