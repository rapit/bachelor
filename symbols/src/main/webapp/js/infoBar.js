function updateInfoBar(obj) {
	if (obj) {
		try {
			var rasterPointSum = obj.rasterPointSum;
			var ratio = obj.ratio;
			var curvaturePointSum = obj.curvaturePointSum;
			var degreeSum = obj.degreeSum;
			var averageDistance = obj.averageDistance;
			var length = obj.length;
			var rasterProp = obj.rasterProp;
			var segments = obj.segments;

			var rasterPointSumBtn = document.getElementById("infoRasterSum");
			if (rasterPointSumBtn) {
				rasterPointSumBtn.innerHTML = rasterPointSum;
			}
			var ratioBtn = document.getElementById("infoAspectRatio");
			if (ratioBtn) {
				ratioBtn.innerHTML = ratio.toFixed(2);
			}
			var curvaturePointSumBtn = document
					.getElementById("infoCountCurvaturePoints");
			if (curvaturePointSumBtn) {
				curvaturePointSumBtn.innerHTML = curvaturePointSum;

			}
			var degreeSumBtn = document.getElementById("infoDegSum");
			if (degreeSumBtn) {
				degreeSumBtn.innerHTML = degreeSum.toFixed(2);

			}
			var averageDistancePointSumBtn = document
					.getElementById("infoPointDistance");
			if (averageDistancePointSumBtn) {
				averageDistancePointSumBtn.innerHTML = averageDistance
						.toFixed(2);

			}
			var lengthBtn = document.getElementById("infoLength");

			if (lengthBtn) {
				lengthBtn.innerHTML = length.toFixed(2);

			}

			var rasterPropBtn = document.getElementById("infoRasterProp");
			if (rasterPropBtn) {
				rasterPropBtn.innerHTML = rasterProp.toFixed(2);

			}
			var segmentsBtn = document.getElementById("infoSegments");
			if (segmentsBtn) {
				segmentsBtn.innerHTML = segments;

			}

		} catch (e) {
			console.log("no valid result");
		}
	}
}

function resetInfoBar() {
	var rasterPointSumBtn = document.getElementById("infoRasterSum");
	if (rasterPointSumBtn) {
		rasterPointSumBtn.innerHTML = "";
	}
	var ratioBtn = document.getElementById("infoAspectRatio");
	if (ratioBtn) {
		ratioBtn.innerHTML = "";

	}
	var curvaturePointSumBtn = document
			.getElementById("infoCountCurvaturePoints");
	if (curvaturePointSumBtn) {
		curvaturePointSumBtn.innerHTML = "";

	}
	var degreeSumBtn = document.getElementById("infoDegSum");
	if (degreeSumBtn) {
		degreeSumBtn.innerHTML = "";

	}
	var averageDistancePointSumBtn = document
			.getElementById("infoPointDistance");
	if (averageDistancePointSumBtn) {
		averageDistancePointSumBtn.innerHTML = "";

	}
	var lengthBtn = document.getElementById("infoLength");
	if (lengthBtn) {
		lengthBtn.innerHTML = "";

	}
	var rasterPropBtn = document.getElementById("infoRasterProp");
	if (rasterPropBtn) {
		rasterPropBtn.innerHTML = "";

	}
	var segmentsBtn = document.getElementById("infoSegments");
	if (segmentsBtn) {
		segmentsBtn.innerHTML = "";

	}
	var acceptedImage = document.getElementById('matchedSymbol');
	if (acceptedImage) {
		acceptedImage.src = "";

	}
}
