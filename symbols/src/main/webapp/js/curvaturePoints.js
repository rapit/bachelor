var line;
var curvaturePoints = new Array();
var sharpness = new Array();

var d_min = 10;
var d_min_sq = d_min * d_min;
var d_max = 2 * d_min;
var d_max_sq = d_max * d_max;
var cos_min = -0.75;

function resetCurvatureValues(dmin, dmax, amax) {
	if (dmin) {
		this.d_min = dmin;

	}
	this.d_min_sq = d_min * d_min;
	if (dmax && dmin && dmax > dmin) {
		this.d_max = dmax;
	} else {
		this.d_max = 2 * d_min;
	}
	this.d_max_sq = d_max * d_max;
	
	if (amax && amax > 0 && amax <= 180) {
        angle = (Math.PI * amax)/180.0; 
        this.cos_min = Math.cos(angle)
	}
}

function calculateCurvaturePoints(symbol) {
	this.line = symbol;

	for (var i = 0; i < this.line.length; i++) {
		sharpness[i] = [ -1, -1, 1000, "empty" ];

		var cos_max = -1;
		var seen_one_upper = false;
		for (var plus = i + 1; plus < this.line.length; plus++) {
			a_sq = squareDistance(i, plus);
			if (seen_one_upper && a_sq > d_max_sq) {
				if (sharpness[i][3] == "empty")
					sharpness[i][3] = "fail upper";
				break;
			}
			if (a_sq < this.d_min_sq) {
				continue;

			}
			seen_one_upper = true;
			var seen_one_lower = false;
			for (var minus = i - 1; minus >= 0; minus--) {
				b_sq = squareDistance(i, minus);
				if (seen_one_lower && b_sq > d_max_sq) {
					if (sharpness[i][3] == "empty")
						sharpness[i][3] = "fail lower";
					break;
				}
				if (b_sq < d_min_sq) {
					continue;

				}
				seen_one_lower = true;
				var c_sq = squareDistance(plus, minus);
				var top = a_sq + b_sq - c_sq;
				var bottom = 2.0 * Math.sqrt(a_sq * b_sq);
				var cos = -2;
				if (bottom != 0.0) {
					cos = top / bottom;
				}
				if (cos < cos_min) {
					if (sharpness[i][3] == "empty") {
						sharpness[i][2] = cos;
						sharpness[i][3] = "angle fail";
					}
					break;
				}
				if (cos_max < cos) {
					cos_max = cos;
					sharpness[i] = [ plus, minus, cos, "OK" ];
				} else {
					if (sharpness[i][3] == "empty") {
						sharpness[i][2] = cos;
						sharpness[i][3] = "angle fail";
					}
				}
			}
		}

	}

	curvaturePoints.length = 0;
	var n_found = -1;
	var last_found = -1;
	for (var i = 0; i < this.line.length; i++) {
		var found = false;
		if (sharpness[i][2] > this.cos_min && sharpness[i][3] == "OK") {
			if (last_found >= 0) {
				var dist_sq = squareDistance(last_found, i);
				if (dist_sq > d_max_sq) {
					n_found++;
					found = true;
				} else if (sharpness[i][2] > sharpness[last_found][2]) {
					found = true;
				}
			} else {
				n_found++;
				found = true;
			}
		}
		if (found) {
			last_found = i;
			curvaturePoints[n_found] = i;
		}
	}
//	for (var j = 0; j < curvaturePoints.length; j++) {
//		console.log(curvaturePoints[j]);
//	}
//	console.log(curvaturePoints);
	return curvaturePoints;
}

function squareDistance(i, j) {
	if (i > 0 && i < this.line.length && j > 0 && j < this.line.length) {
		var i = this.line[i];
		var j = this.line[j];
		var xDiff = j[0] - i[0];
		var yDiff = j[1] - i[1];
		return (xDiff * xDiff + yDiff * yDiff);
	}
}
