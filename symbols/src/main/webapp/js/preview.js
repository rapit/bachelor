function getGrid() {
	var gridFactor = globalGridFactor; // width mod gridFactor = 0 and
	// height mod gridFactor = 0
	var width = canvas.width;
	var height = canvas.height;
	var imageData = context.getImageData(0, 0, width, height);

	if (document.getElementById('checkboxScale').checked) {
		imageData = resizeImageData(canvas, this.symbolPoints, width);
	}

	var gridData = new Array(width / gridFactor * height / gridFactor);
	var data = imageData.data;
	var sum = 0;

	for (var i = 0; i < gridData.length; ++i) {
		gridData[i] = false;
	}

	for (var y = 0; y < height; y += gridFactor) {
		for (var x = 0; x < width; x += gridFactor) {
			sum = 0;
			for (var y1 = 0; y1 < gridFactor; y1++) {
				for (var x1 = 0; x1 < gridFactor; x1++) {
					sum += data[((y + y1) * width + (x + x1)) * 4 + 3];
				}
			}
			sum /= 255;
			if (sum > (canvas.width * canvas.height / (gridFactor * gridFactor) / 10)) {
				var index = ((y / gridFactor * width / gridFactor) + x / gridFactor);
				// console.log(index);
				gridData[index] = true;
			}

		}
	}
	return {
		data : gridData,
		factor : gridFactor
	};

}

function createHammingPreview(gridData, gridFactor, previewCompare) {
	// var bytesHamming = new Uint8Array(625);
	if (previewCompare >= 0) {
		var compareObj = JSONdata[previewCompare].hammingcontent;
		var reader = new window.FileReader();
		var bytesHamming = new Uint8Array(compareObj);

	}
	var preCanvas = document.getElementById('previewCanvas');
	var preContext = preCanvas.getContext("2d");
	preContext.clearRect(0, 0, preContext.canvas.width,
			preContext.canvas.height);

	var width = canvas.width;
	var height = canvas.height;
	var newImageData = context.createImageData(preCanvas.width,
			preCanvas.height);
	var xFactor = preCanvas.width / (width / gridFactor);
	var yFactor = preCanvas.height / (height / gridFactor);
	// console.log(xFactor);
	// console.log(yFactor);

	for (var y = 0; y < height / gridFactor; y++) {
		for (var x = 0; x < width / gridFactor; x++) {
			if ((gridData[y * (width / gridFactor) + x]) // only compare
					&& (previewCompare >= 0)
					&& (((bytesHamming[y * (width / gridFactor) + x] - 48)) != 0)) {
				for (var xd = 0; xd < xFactor; xd++) {
					for (var yd = 0; yd < yFactor; yd++) {

						var index = ((((y * yFactor + yd) * (width / gridFactor * xFactor)) + (x
								* xFactor + xd)) * 4 + 3);
						newImageData.data[index - 1] = 255;
						newImageData.data[index] = 255;
					}
				}
			} else if (gridData[y * (width / gridFactor) + x]) { // only
				// original
				for (var xd = 0; xd < xFactor; xd++) {
					for (var yd = 0; yd < yFactor; yd++) {

						var index = ((((y * yFactor + yd) * (width / gridFactor * xFactor)) + (x
								* xFactor + xd)) * 4 + 3);

						newImageData.data[index] = 255;
					}
				}
			} else if ((previewCompare >= 0) // both
					&& (((bytesHamming[y * (width / gridFactor) + x] - 48)) != 0)) {
				for (var xd = 0; xd < xFactor; xd++) {
					for (var yd = 0; yd < yFactor; yd++) {

						var index = ((((y * yFactor + yd) * (width / gridFactor * xFactor)) + (x
								* xFactor + xd)) * 4 + 3);
						newImageData.data[index - 2] = 255;
						newImageData.data[index] = 255;
					}
				}
			}

		}
	}

	preContext.putImageData(newImageData, 0, 0);
}

function clearPreview() {
	var preCanvas = document.getElementById('previewCanvas');
	var preContext = preCanvas.getContext("2d");
	preContext.clearRect(0, 0, preContext.canvas.width,
			preContext.canvas.height);
}


function createRotadedPointComparePreview(pointData, width, height, previewCompare) {
	var preCanvas = document.getElementById('previewCanvas');
	var preContext = preCanvas.getContext("2d");
	preContext.clearRect(0, 0, preContext.canvas.width,
			preContext.canvas.height);

	var widthFactor = (preCanvas.width - 20) / width;
	var heightFactor = (preCanvas.height - 20) / height;

	if (previewCompare >= 0) {
		var compareRotation = document.getElementById("compareRotation");
		if (compareRotation) {
			compareRotation.innerHTML = JSONdata[previewCompare].rotation;
		}
		
		var pointString = JSONdata[previewCompare].normalizedPoints;
		if (pointString) {
			var splitString = pointString.split("|");
			preContext.fillStyle = "#000000";

			for (var i = 0; i < splitString.length; i++) {
				var point = splitString[i].split(",");
				// console.log(point);
				if (point.length == 2) {
					var x = parseInt(point[0]) * widthFactor;
					var y = parseInt(point[1]) * heightFactor;
					// console.log(x + " | " + y );
					preContext.fillRect(x + 10, y + 10, 5, 5);

				}

			}
		}
		

	}
	if (JSONdata != null && (previewCompare == -1)) {

		if (JSONdata[JSONdata.length - 1]) {
			try {
				var minX = JSONdata[JSONdata.length - 1].minX;
				var minY = JSONdata[JSONdata.length - 1].minY;
				var xOffset = JSONdata[JSONdata.length - 1].xOffset;
				var yOffset = JSONdata[JSONdata.length - 1].yOffset;
				var scaleFactor = JSONdata[JSONdata.length - 1].scaleFactor;
//				console.log(minX + "|" + minY + "|" + xOffset + "|" + yOffset
//						+ "|" + scaleFactor);
				preContext.fillStyle = "#FF0000";
				for (var i = 0; i < this.curvaturePoints.length; i++) {
					var x = ((this.symbolPoints[this.curvaturePoints[i]][0] - minX)
							* scaleFactor + xOffset)
							* widthFactor;
					var y = ((this.symbolPoints[this.curvaturePoints[i]][1] - minY)
							* scaleFactor + yOffset)
							* heightFactor;
					preContext.fillRect(x + 10, y + 10, 5, 5);

				}
				preContext.fillStyle = "#0000FF";
				for (var i = 0; i < this.startEndPoints.length; i++) {
					var x = ((this.startEndPoints[i][0] - minX) * scaleFactor + xOffset)
							* widthFactor;
					var y = ((this.startEndPoints[i][1] - minY) * scaleFactor + yOffset)
							* heightFactor;
					preContext.fillRect(x + 10, y + 10, 5, 5);

				}

			} catch (e) {
				console.log("no valid result");
			}
		}

	} else {
		var pointString = JSONdata[previewCompare].rotatedInputPoints;
		var splitString = pointString.split("|");
		preContext.fillStyle = "#FF0000";

		for (var i = 0; i < splitString.length; i++) {
			var point = splitString[i].split(",");
			// console.log(point);
			if (point.length == 2) {
				var x = parseInt(point[0]) * widthFactor;
				var y = parseInt(point[1]) * heightFactor;
				// console.log(x + " | " + y );
				preContext.fillRect(x + 10, y + 10, 5, 5);

			}

		}
	}

}

function createPointComparePreview(pointData, width, height, previewCompare) {
	var preCanvas = document.getElementById('previewCanvas');
	var preContext = preCanvas.getContext("2d");
	preContext.clearRect(0, 0, preContext.canvas.width,
			preContext.canvas.height);

	var widthFactor = (preCanvas.width - 20) / width;
	var heightFactor = (preCanvas.height - 20) / height;

	if (previewCompare >= 0) {
		var pointString = JSONdata[previewCompare].normalizedPoints;
		var splitString = pointString.split("|");
		preContext.fillStyle = "#000000";

		for (var i = 0; i < splitString.length; i++) {
			var point = splitString[i].split(",");
			// console.log(point);
			if (point.length == 2) {
				var x = parseInt(point[0]) * widthFactor;
				var y = parseInt(point[1]) * heightFactor;
				// console.log(x + " | " + y );
				preContext.fillRect(x + 10, y + 10, 5, 5);

			}

		}
	}
	if (JSONdata != null) {

		if (JSONdata[JSONdata.length - 1]) {
			try {
				var minX = JSONdata[JSONdata.length - 1].minX;
				var minY = JSONdata[JSONdata.length - 1].minY;
				var xOffset = JSONdata[JSONdata.length - 1].xOffset;
				var yOffset = JSONdata[JSONdata.length - 1].yOffset;
				var scaleFactor = JSONdata[JSONdata.length - 1].scaleFactor;
				
				var rotation = document.getElementById("rotation");
				if (rotation) {
					rotation.innerHTML = JSONdata[JSONdata.length - 1].rotation.toFixed(2);
				}
				
//				console.log(minX + "|" + minY + "|" + xOffset + "|" + yOffset
//						+ "|" + scaleFactor);
				preContext.fillStyle = "#FF0000";
				for (var i = 0; i < this.curvaturePoints.length; i++) {
					var x = ((this.symbolPoints[this.curvaturePoints[i]][0] - minX)
							* scaleFactor + xOffset)
							* widthFactor;
					var y = ((this.symbolPoints[this.curvaturePoints[i]][1] - minY)
							* scaleFactor + yOffset)
							* heightFactor;
					preContext.fillRect(x + 10, y + 10, 5, 5);

				}
				preContext.fillStyle = "#0000FF";
				for (var i = 0; i < this.startEndPoints.length; i++) {
					var x = ((this.startEndPoints[i][0] - minX) * scaleFactor + xOffset)
							* widthFactor;
					var y = ((this.startEndPoints[i][1] - minY) * scaleFactor + yOffset)
							* heightFactor;
					preContext.fillRect(x + 10, y + 10, 5, 5);

				}

			} catch (e) {
				console.log("no valid result");
			}
		}

	}

}