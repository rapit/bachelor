var self = this;

var connection = new WebSocket('ws://localhost:8080/classify');

connection.onopen = function () {
	  connection.send('Ping'); // Send the message 'Ping' to the server
};

connection.onerror = function (error) {
	  console.log('WebSocket Error ' + error);
};

connection.onmessage = function (e) {
	var d = new Date();
	var textline = "\n" + d + ": " + e.data;
	document.getElementById('textbox').value += textline;
};

this.sendRequest = function() {
	request = document.getElementById('msg').value;
	connection.send(request);
	document.getElementById('msg').value = "";
}