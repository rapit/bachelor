package org.example;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public class ClassificationSocket {
	
    @OnWebSocketMessage
    public void onMessage(Session session, String msg)
    {
        session.getUpgradeRequest().getParameterMap();
        session.getRemote().sendStringByFuture("'" + msg + "' echo from " + this.getClass().getName());
       
    }
    
    @OnWebSocketConnect
    public void onWebSocketConnect(Session sess)
    {
        System.out.println("Socket Connected: " + sess);
    }
    
    
    @OnWebSocketClose
    public void onWebSocketClose(int i, String s)
    {
        System.out.println("Socket Closed");
    }
    
    @OnWebSocketError
    public void onWebSocketError(Throwable cause)
    {
        cause.printStackTrace(System.err);
    }
}
