package org.example;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;

public class ImageScaleInstance {
	private ArrayList<Point2D> featurePoints = null;
	private double minX = 0.0;
	private double minY = 0.0;
	private double maxX = 0.0;
	private double maxY= 0.0;
	private double scaleFactor = 1.0;
	private double xOffset = 0.0;
	private double yOffset = 0.0;
	private double ratio = 1.0;
	private double length = 0.0;

	public ImageScaleInstance(ArrayList<Point2D> featurePoints, double minX, double minY, double maxX, double maxY,
			double scaleFactor, double xOffset, double yOffset, double length) {
		this.featurePoints = featurePoints;
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.scaleFactor = scaleFactor;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		if ((maxY - minY) != 0) {
			this.ratio = (maxX - minX) / (maxY - minY);
		}
		this.length = length;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

	public ArrayList<Point2D> getFeaturePoints() {
		return featurePoints;
	}

	public void setFeaturePoints(ArrayList<Point2D> featurePoints) {
		this.featurePoints = featurePoints;
	}

	public double getMinX() {
		return minX;
	}

	public void setMinX(double minX) {
		this.minX = minX;
	}
	
	public double getMinY() {
		return minY;
	}

	public void setMinY(double minY) {
		this.minY = minY;
	}

	public double getMaxX() {
		return maxX;
	}

	public void setMaxX(double maxX) {
		this.maxX = maxX;
	}

	public double getMaxY() {
		return maxY;
	}

	public void setMaxY(double maxY) {
		this.maxY = maxY;
	}

	public double getScaleFactor() {
		return scaleFactor;
	}

	public void setScaleFactor(double scaleFactor) {
		this.scaleFactor = scaleFactor;
	}

	public double getxOffset() {
		return xOffset;
	}

	public void setxOffset(double xOffset) {
		this.xOffset = xOffset;
	}

	public double getyOffset() {
		return yOffset;
	}

	public void setyOffset(double yOffset) {
		this.yOffset = yOffset;
	}


}
