package org.example;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.Vote;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.OneR;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.LMT;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.RandomTree;
import weka.core.Debug.Random;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.Tag;
import weka.core.neighboursearch.LinearNNSearch;

public class Classificators {
	public static Classifier generateClassifier(Instances i, int index) {

		Classifier cModel = null;

		switch (index) {
		case 0: {
			cModel = (Classifier) new J48();
			break;
		}
		case 1: {
			cModel = (Classifier) new RandomTree();
			break;
		}
		case 2: {
			cModel = (Classifier) new LMT();
			break;
		}
		case 3: {
			cModel = (Classifier) new NaiveBayes();
			break;
		}
		case 4: {
			cModel = (Classifier) new MultilayerPerceptron();
			break;
		}
		case 5: {
			cModel = (Classifier) new DecisionTable();
			break;
		}
		case 6: {
			cModel = (Classifier) new OneR();
			break;
		}
		case 7: {
			cModel = (Classifier) new RandomForest();
			break;
		}
		case 8: {
			cModel = (Classifier) new IBk(3);
			break;
		}
		case 9: {
//			Vote v = new Vote();
//			v.addPreBuiltClassifier(new NaiveBayes());
//			v.addPreBuiltClassifier(new MultilayerPerceptron());
//			v.addPreBuiltClassifier(new J48());
//			final int AVERAGE_RULE = 1;
//			final Tag[] TAGS_RULES = {
//				    new Tag(AVERAGE_RULE, "AVG", "Average of Probabilities"),
//				  };
//				  
//			v.setCombinationRule(new SelectedTag(AVERAGE_RULE, TAGS_RULES));
			cModel = (Classifier) new  RandomForest() ;
			break;

		}
		default: {
			cModel = (Classifier) new RandomForest();
		}

		}
		try {
			cModel.buildClassifier(i);
//			if (index == 4) {
//				System.out.println("Start MLP");
//				 Evaluation eval = new Evaluation(i);
//				 Random rand = new Random(1);  // using seed = 1
//				 int folds = 10;
//				 eval.crossValidateModel(cModel, i, folds, rand);
//				 System.out.println(eval.toSummaryString());
//				System.out.println("End MLP");
//
//					}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cModel;
	}
}
