package org.example;

public final class SQLIdentifier {
	public static final String DATABASE = "java:comp/env/jdbc/myds";

	
	public static final String DATABASE_NAME_MAIN = "images";
	
	public static final String IMAGES_ID = "id";
	public static final String IMAGES_DATE = "date";
	public static final String IMAGES_NAME = "name";
	public static final String IMAGES_SVG = "svg";
	public static final String IMAGES_CONTENT = "content";
	
	public static final String DATABASE_NAME_HAMMING = "hamming";
	public static final String HAMMING_ID = "id";
	public static final String HAMMING_CONTENT = "content";
	
	public static final String DATABASE_NAME_NORMALIZED = "normalized";
	public static final String NORMALIZED_ID = "id";
	public static final String NORMALIZED_IMAGE = "image";
	
	public static final String DATABASE_NAME_SVG = "svg";
	public static final String SVG_ID = "id";
	public static final String SVG_WIDTH = "width";
	public static final String SVG_HEIGHT = "height";
	public static final String SVG_POINTS = "points";
	public static final String SVG_NORMALIZEDPOINTS = "normalizedPoints";
	
	public static final String DATABASE_NAME_TRAINING = "training";
	public static final String TRAINING_UNIQUE_ID = "uniqueID";
	public static final String TRAINING_CLASS_ID = "classID";
	public static final String TRAINING_RASTER625 = "raster625";
	public static final String TRAINING_RASTER_POINTSUM = "rasterPointSum";
	public static final String TRAINING_RATIO = "ratio";
	public static final String TRAINING_CURVATURE_POINTSUM = "curvaturePointSum";
	public static final String TRAINING_DEGREE_SUM = "degreeSum";
	public static final String TRAINING_AVERGAE_DISTANCE = "averageDistance";
	public static final String TRAINING_IMAGE = "image";
	public static final String TRAINING_LENGTH = "length";
	public static final String TRAINING_RASTER_PROP = "rasterProp";
	public static final String TRAINING_SEGMENTS = "segments";
	public static final String TRAINING_WEIGHT = "weight";

	public static final String TRAINING_GRAVITY_X = "gravityX";
	public static final String TRAINING_GRAVITY_Y = "gravityY";
	public static final String TRAINING_CURVERASTER = "curveraster";
	
	public static final String PROBABILITY = "prob";
	
	public static final String DATABASE_NAME_GEO = "geo";
	public static final String GEO_IDGEO = "geoId";
	public static final String GEO_ID = "id";
	public static final String GEO_IMAGE = "image";
	public static final String GEO_WIDTH = "width";
	public static final String GEO_HEIGHT = "height";
	public static final String GEO_NORMALIZEDPOINTS = "normalizedPoints";
	public static final String GEO_POINTS = "points";
	public static final String GEO_BINARY = "hammingcontent";
	public static final String GEO_KIND = "kind";
	public static final String GEO_AREA = "area";
	public static final String GEO_ADRESS = "addrStreet";
	public static final String GEO_BUILDING_HEIGHT = "heightBuilding";
	public static final String GEO_HOUSE_NUMBER = "housenumber";


	
	
}
