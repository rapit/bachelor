package org.example;

import java.awt.geom.Point2D;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.util.AffineTransformation;

import weka.classifiers.Classifier;
import weka.core.Instances;

public class GeoRanker extends PointRanker {
	
	public JSONArray rank(ResultSet rs, ArrayList<Point2D> featurePoints, boolean invarianceFlag, int nearestPointDistance) throws JSONException, SQLException {
		String typeID = null;
		String typePoints = null;
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			typeID = rsmd.getColumnTypeName(1);
			typePoints = rsmd.getColumnTypeName(3);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<Point2D> databasePoints = null;

		List<JSONObject> jsonValues = new ArrayList<JSONObject>();
		JSONArray sortedResultJson = new JSONArray();
		int degStep = 20;

		if (!invarianceFlag) {
			degStep = 360;
		}

		while (rs.next()) {
			for (int rotDeg = 0; rotDeg < 360; rotDeg += degStep) {
				JSONObject obj = new JSONObject();
				if (typeID == "INT") {
					try {
						obj.put(SQLIdentifier.GEO_ID, rs.getInt(1));
					} catch (JSONException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (typePoints == "VARCHAR") {
					String points = rs.getString(3);
					databasePoints = this.parsePoints(points);
					
					if (nearestPointDistance > 0) {
					
						databasePoints = this.nearPointFilter(databasePoints, nearestPointDistance);
						featurePoints = this.nearPointFilter(featurePoints, nearestPointDistance);
					}
					
					double prob = 0.0;
					// System.out.println("database: " + databasePoints.size() +
					// " | feature: " + featurePoints.size());
					ArrayList<Point2D> rotFeaturePoints = this.rotateFeaturePoints(featurePoints, rotDeg);
					String rotPointString = "";
					for (int j = 0; j < rotFeaturePoints.size(); j++) {
						rotPointString += rotFeaturePoints.get(j).getX() + "," + rotFeaturePoints.get(j).getY() + "|";
					}
					
					obj.put("rotatedPoints", rotPointString);
					if (databasePoints.size() > 0 && rotFeaturePoints.size() > 0 && invarianceFlag) {
						prob = matchNearestPoints(databasePoints, rotFeaturePoints);
					} else {
						prob = matchNearestPoints(databasePoints, featurePoints);

					}
					//
					obj.put(this.cNameProb, prob);
//					 System.out.println("ID:" +rs.getInt(1) + " | Rotation" + rotDeg + " | Prob: " + prob);
//					 System.out.println("--------------------");

				}
				obj.put(this.cNameRotation, rotDeg);

				// obj.put(this.cNameProb, Math.random() * 100);
				jsonValues.add(obj);
			}
		}
		Collections.sort(jsonValues, this.probCompare);
		ArrayList<Integer> blackList = new ArrayList<Integer>();
		int count = 0;
		int found = 0;
		while (found < Math.min(10, jsonValues.size())) {
			JSONObject obj = jsonValues.get(count);
			int id = (int) obj.get(SQLIdentifier.GEO_ID);
			boolean inBlackList = false;
			for (Integer i : blackList) {
				if (id == i) {
					inBlackList = true;
				}
			}
			if (!inBlackList) {
				sortedResultJson.put(obj);
				blackList.add(id);
				found++;
			}

			count++;

		}
//		 for (int i = 0; i < Math.min(10, jsonValues.size()); i++) { //
//			 sortedResultJson.put(jsonValues.get(i));
//		 	System.out.println(jsonValues.get(i).get(this.cNameRotation));
//		 }
		return sortedResultJson;

	}
	

	
	
	

}
