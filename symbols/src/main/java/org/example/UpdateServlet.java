package org.example;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.example.util.ThinningService;
import org.example.util.ZhangSuen;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.wololo.geojson.Feature;
import org.wololo.geojson.FeatureCollection;
import org.wololo.geojson.GeoJSONFactory;
import org.wololo.jts2geojson.GeoJSONReader;

import com.mysql.jdbc.PreparedStatement;
import com.vividsolutions.jts.awt.ShapeWriter;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.util.AffineTransformation;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

public class UpdateServlet extends HttpServlet {

	private static final String DATABASE = "java:comp/env/jdbc/myds";

	private static final String FILE_LOCATION_PATH = "src/main/resources/raw";
	private static final int NORMALIZED_SIZE = 500;

	private int RASTER_WIDTH = 25;
	private int DISTANCE_TOLERANCE = 10;
	private static final boolean THINNING = true;

	public UpdateServlet() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String a = request.getParameter("grid");
		this.RASTER_WIDTH = Integer.valueOf(a);
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("success", "yes");
		updateMainDatabase();
		System.out.println("successfully updated database");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StringBuffer jb = new StringBuffer();
		String line = null;

		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);

			}
		} catch (IOException | IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JSONObject jsonObject = null;
		// try {
		// jsonObject = new JSONObject(jb.toString());
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		updateGeoDatabase(jb.toString());

		response.setHeader("Content-Type", "text/plain");
		response.setHeader("success", "yes");
		PrintWriter writer = response.getWriter();
		writer.write("updated Geo");
		writer.close();
		System.out.println("updating geoDatabase");

	}

	private void updateGeoDatabase(String jsonObject) {
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(this.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;
		try {
			conn = myDS.getConnection();

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM " + SQLIdentifier.DATABASE_NAME_GEO);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FeatureCollection featureCollection = (FeatureCollection) GeoJSONFactory.create(jsonObject);
		GeoJSONReader reader = new GeoJSONReader();
		int i = 0;
		for (Feature f : featureCollection.getFeatures()) {
			i++;
			Geometry geometry = reader.read(f.getGeometry());

			HashMap<String, Object> props = new HashMap<String, Object>(f.getProperties());

			int id = 200 + i;
			String geoid = "";
			if (props.get("id") != null) {
				geoid = props.get("id").toString();
			}

			String kind = "-";
			if (props.get("kind") != null) {
				kind = (String) props.get("kind");
			}

			String adress = "-";
			if (props.get("addr_street") != null) {
				adress = (String) props.get("addr_street");
			}

			String housenumber = "-";
			if (props.get("addr_housenumber") != null) {
				housenumber = (String) props.get("addr_housenumber");
			}

			double area = 0.0;
			if (props.get("area") != null) {
				area = ((Integer) props.get("area")).doubleValue();
			}
			double buildingheight = 0.0;
			if (props.get("height") != null) {
				buildingheight = (double) props.get("height");
			}

			String pointsString = "";
			String normalizedPointString = "";
			Coordinate[] pts = geometry.getCoordinates();

			double minX = 0.5 * Math.PI;
			double maxX = -0.5 * Math.PI;

			double minY = Math.PI;
			double maxY = -Math.PI;

			for (Coordinate p : pts) {
				double x = Math.toRadians(p.x);
				double y = Math.toRadians(p.y);

				if (x < minX) {
					minX = x;
				}
				if (x > maxX) {
					maxX = x;
				}
				if (y < minY) {
					minY = y;
				}
				if (y > maxY) {
					maxY = y;
				}
			}

			// System.out.println(minX + "|" + maxX + "|" + minY + "|" + maxY);

			int width = 500;
			int height = 500;

			minY = this.mercY(minY);
			maxY = this.mercY(maxY);

			double factor = 1;
			if (Math.max(maxX - minX, maxY - minY) != 0) {
				factor = width / Math.max(maxX - minX, maxY - minY);
			}
			double xOffset = (width - (maxX - minX) * factor) / 2.0;
			double yOffset = (height - (maxY - minY) * factor) / 2.0;
			// System.out.println(factor);

			Coordinate[] scaledPts = new Coordinate[pts.length];

			for (int j = 0; j < scaledPts.length; j++) {
				Coordinate p = pts[j];
				double x = Math.toRadians(p.x);
				double y = this.mercY(Math.toRadians(p.y));
				x = (x - minX) * factor + xOffset;
				y = (maxY - y) * factor + yOffset;
				scaledPts[j] = new Coordinate(x, y);
			}
			GeometryFactory geometryFactory = new GeometryFactory();
			BufferedImage image = null;
			if (scaledPts.length == 1) {
				Point scaledPoint = geometryFactory.createPoint(scaledPts[0]);
				image = getImageFromGeometry(scaledPoint, width, height);
				pointsString += scaledPts[0].x + "," + scaledPts[0].y + "|";

			} else {
				LineString lineString = geometryFactory.createLineString(scaledPts);

				if (this.DISTANCE_TOLERANCE > 0) {
					LineString simpleLineString = (LineString) DouglasPeuckerSimplifier.simplify(lineString,
							DISTANCE_TOLERANCE);
					for (int j = 0; j < simpleLineString.getNumPoints(); j++) {
						pointsString += simpleLineString.getCoordinateN(j).x + ","
								+ simpleLineString.getCoordinateN(j).y + "|";
					}
				} else {
					for (int j = 0; j < lineString.getNumPoints(); j++) {
						pointsString += lineString.getCoordinateN(j).x + "," + lineString.getCoordinateN(j).y + "|";
					}
				}

				if (lineString.isClosed()) {
					Polygon scaledPolygon = geometryFactory.createPolygon(lineString.getCoordinateSequence());
					image = getImageFromGeometry(scaledPolygon, width, height);
				} else {
					image = getImageFromGeometry(lineString, width, height);

				}

			}

			boolean[] rasterData = getRasterData(image);
			String hammingRaster = "";

			for (int z = 0; z < rasterData.length; ++z) {
				if (rasterData[z]) {
					hammingRaster = hammingRaster + "1";
				} else {
					hammingRaster = hammingRaster + "0";

				}
			}
						// System.out.println(hammingRaster);
			// System.out.println(hammingRaster.length());
			addGeoElement(id, geoid, image, (double) image.getWidth(), (double) image.getHeight(), pointsString,
					hammingRaster, kind, area, adress, housenumber, buildingheight);

		}

	}

	private double mercY(double lat) {
		return Math.log(Math.tan(lat / 2 + Math.PI / 4));
	}

	private BufferedImage getImageFromGeometry(Geometry geometry, int width, int height) {

		ShapeWriter sw = new ShapeWriter();
		Shape polyShape = sw.toShape(geometry);
		BufferedImage image = new BufferedImage(width, height, 6);
		Graphics2D g2d = image.createGraphics();
		g2d.setColor(new Color(0, 0, 0, 1));
		g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
		g2d.setColor(Color.BLACK);
		g2d.setStroke(new BasicStroke(10));
		g2d.draw(polyShape);
		g2d.dispose();

		return image;
	}

	private void addGeoElement(int id, String geoid, BufferedImage image, double width, double height, String points,
			String binaryData, String kind, double area, String adress, String houseNumber, double buildingHeight) {
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(this.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;
		try {
			conn = myDS.getConnection();

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream binStream = new ByteArrayInputStream(binaryData.getBytes(StandardCharsets.UTF_8));

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			ImageIO.write(image, "png", os);

			is = new ByteArrayInputStream(os.toByteArray());

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String INSERT_ELEMENT = "insert into " + SQLIdentifier.DATABASE_NAME_GEO + " (" + SQLIdentifier.GEO_ID + ","
				+ SQLIdentifier.GEO_IDGEO + "," + SQLIdentifier.GEO_IMAGE + "," + SQLIdentifier.GEO_WIDTH + ","
				+ SQLIdentifier.GEO_HEIGHT + "," + SQLIdentifier.GEO_NORMALIZEDPOINTS + "," + SQLIdentifier.GEO_BINARY
				+ "," + SQLIdentifier.GEO_KIND + "," + SQLIdentifier.GEO_AREA + "," + SQLIdentifier.GEO_ADRESS + ","
				+ SQLIdentifier.GEO_HOUSE_NUMBER + "," + SQLIdentifier.GEO_BUILDING_HEIGHT
				+ ") values (?,?,?,?,?,?,?,?,?,?,?,?);";
		java.sql.PreparedStatement ps = null;

		try {
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(INSERT_ELEMENT);
			ps.setInt(1, id);
			ps.setString(2, geoid);
			ps.setBinaryStream(3, is, (int) os.size());
			ps.setDouble(4, width);
			ps.setDouble(5, height);
			ps.setString(6, points);
			ps.setBinaryStream(7, binStream, (int) binaryData.getBytes().length);
			ps.setString(8, kind);
			ps.setDouble(9, area);
			ps.setString(10, adress);
			ps.setString(11, houseNumber);
			ps.setDouble(12, buildingHeight);
			ps.executeUpdate();
			conn.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}

		}
	}

	private void updateHammingDatabase(BufferedImage image, String id) {
		int w = image.getWidth();
		int h = image.getHeight();

		if (image.getType() != 6) {
			BufferedImage convertImage = new BufferedImage(w, h, 6);
			convertImage.getGraphics().drawImage(image, 0, 0, null);
			image = convertImage;
		}

		BufferedImage scaledImage = new BufferedImage(NORMALIZED_SIZE, NORMALIZED_SIZE, 6);
		// System.out.println("ID: " + id + "| Type: " + image.getType());
		AffineTransform scaleAT = new AffineTransform();
		double scaleFactor = (double) Math.round(NORMALIZED_SIZE / (double) Math.max(w, h) * 10) / 10;
		// System.out.println("ID: " + id + " | ScaleFactor: " + scaleFactor);
		scaleAT.scale(scaleFactor, scaleFactor);

		AffineTransformOp scaleOp = new AffineTransformOp(scaleAT, AffineTransformOp.TYPE_BILINEAR);
		scaledImage = scaleOp.filter(image, scaledImage);

		AffineTransform translateAT = new AffineTransform();
		if (w >= h) {
			translateAT.translate(0, Math.round((NORMALIZED_SIZE - h * scaleFactor) * 0.5));
			double print = Math.round((NORMALIZED_SIZE - h * scaleFactor) * 0.5);
			// System.out.println("ID: " + id + " | TranslateFactor: " + print);

		} else {
			translateAT.translate(Math.round((NORMALIZED_SIZE - w * scaleFactor) * 0.5), 0);
			double print = Math.round((NORMALIZED_SIZE - w * scaleFactor) * 0.5);

			// System.out.println("ID: " + id + " | TranslateFactor: " + print);

		}
		AffineTransformOp translateOP = new AffineTransformOp(translateAT, AffineTransformOp.TYPE_BILINEAR);

		BufferedImage normalizedImage = new BufferedImage(NORMALIZED_SIZE, NORMALIZED_SIZE, 6);

		normalizedImage = translateOP.filter(scaledImage, normalizedImage);

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			ImageIO.write(normalizedImage, "png", os);

			is = new ByteArrayInputStream(os.toByteArray());

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(this.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		try {
			conn = myDS.getConnection();

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String INSERT_ELEMENT = "insert into " + SQLIdentifier.DATABASE_NAME_NORMALIZED + " ("
				+ SQLIdentifier.NORMALIZED_ID + "," + SQLIdentifier.NORMALIZED_IMAGE + ") values (?,?)";

		java.sql.PreparedStatement ps = null;

		try {
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(INSERT_ELEMENT);
			ps.setInt(1, Integer.valueOf(id));
			ps.setBinaryStream(2, is, (int) os.size());

			ps.executeUpdate();
			conn.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				os.close();
				is.close();
			} catch (SQLException | IOException e) {
				e.printStackTrace();

			}

		}
		boolean[] rasterData = null;

		String hammingRaster = "";

		if (id.equals("1020")) {
//			System.out.println("1020");
			hammingRaster = org.example.util.StaticHamming.getStaticHamming(0);
		} else if (id.equals("1018")) {
			hammingRaster = org.example.util.StaticHamming.getStaticHamming(1);
			
		} else if (id.equals("1006")) {
			hammingRaster = org.example.util.StaticHamming.getStaticHamming(3);

		} else if (id.equals("1001")) {
			hammingRaster = org.example.util.StaticHamming.getStaticHamming(4);

		} else if (id.equals("1008")) {
			hammingRaster = org.example.util.StaticHamming.getStaticHamming(5);
		} else {
			rasterData = getRasterData(normalizedImage);
			for (int i = 0; i < rasterData.length; ++i) {
				if (rasterData[i]) {
					hammingRaster = hammingRaster + "1";
				} else {
					hammingRaster = hammingRaster + "0";

				}
			}

		}

		InputStream stream = new ByteArrayInputStream(hammingRaster.getBytes(StandardCharsets.UTF_8));

		String INSERT_ELEMENT_HAMMING = "insert into " + SQLIdentifier.DATABASE_NAME_HAMMING + " ("
				+ SQLIdentifier.HAMMING_ID + "," + SQLIdentifier.HAMMING_CONTENT + ") values (?,?)";

		ps = null;

		try {
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(INSERT_ELEMENT_HAMMING);
			ps.setInt(1, Integer.valueOf(id));
			ps.setBinaryStream(2, stream, (int) hammingRaster.getBytes().length);

			ps.executeUpdate();
			conn.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				stream.close();
			} catch (SQLException | IOException e) {
				e.printStackTrace();

			}

		}
		// System.out.println(hammingRaster);

	}

	private boolean[] getRasterData(BufferedImage img) {
		boolean[] raster = new boolean[this.RASTER_WIDTH * this.RASTER_WIDTH];

		int width = img.getWidth();
		int height = img.getHeight();

		int gridFactor = width / this.RASTER_WIDTH;
		int sum = 0;

		Raster alphaRaster = img.getAlphaRaster();
		if (raster != null) {
			int[] alphaPixel = new int[alphaRaster.getNumBands()];

			for (int i = 0; i < raster.length; i++) {
				raster[i] = false;
			}

			for (int y = 0; y < height; y += gridFactor) {
				for (int x = 0; x < width; x += gridFactor) {
					sum = 0;
					for (int y1 = 0; y1 < gridFactor; y1++) {
						for (int x1 = 0; x1 < gridFactor; x1++) {
							alphaRaster.getPixel((x + x1), (y + y1), alphaPixel);


							if (alphaPixel[0] != 0x00 && alphaPixel[0] != 1) {
								sum += 1;
							}
						}
					}
					if (sum > (width * height / (gridFactor * gridFactor) / 10)) {
						int index = ((y / gridFactor * width / gridFactor) + x / gridFactor);
						raster[index] = true;
					}

				}
			}
		}
		if (UpdateServlet.THINNING) {
			int[][] rasterArray = new int[this.RASTER_WIDTH][this.RASTER_WIDTH];
			for (int i = 0; i < this.RASTER_WIDTH; i++) {
				for (int j = 0; j < this.RASTER_WIDTH; j++) {
					if (raster[i + (j * i)]) {
						rasterArray[i][j] = 1;
					} else {
						rasterArray[i][j] = 0;

					}

				}
			}

			ThinningService thinning = new ThinningService();
			int[][] skinnedRaster = thinning.doZhangSuenThinning(rasterArray,false);

			for (int i = 0; i < this.RASTER_WIDTH; i++) {
				for (int j = 0; j < this.RASTER_WIDTH; j++) {
					if (skinnedRaster[i][j] == 1) {
						raster[i + (j * i)] = true;
					} else {
						raster[i + (j * i)] = false;

					}

				}
			}
		}

		return raster;
	}

	private void updateMainDatabase() {

		File dir = new File(FILE_LOCATION_PATH);
		int idCount = 1000;
		String name = "";
		Date utilDate = new Date();
		String path = "";

		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		String INSERT_ELEMENT = "insert into " + SQLIdentifier.DATABASE_NAME_MAIN + " (" + SQLIdentifier.IMAGES_ID + ","
				+ SQLIdentifier.IMAGES_DATE + "," + SQLIdentifier.IMAGES_NAME + "," + SQLIdentifier.IMAGES_CONTENT + ","
				+ SQLIdentifier.IMAGES_SVG + ") values (?,?,?,?,?)";
		FileInputStream fisContent = null;
		FileInputStream fisSVG = null;

		java.sql.PreparedStatement ps = null;

		java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

		if (dir.isDirectory()) {
			String[] ext = new String[] { "png" };
			List<File> files = (List<File>) FileUtils.listFiles(dir, ext, true);
			Collections.sort(files, new Comparator<File>() {
				public int compare(File f1, File f2) {
					return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
				}
			});
			try {
				conn = myDS.getConnection();
				stmt = conn.createStatement();
				stmt.executeUpdate("DELETE FROM " + SQLIdentifier.DATABASE_NAME_HAMMING);
				stmt.executeUpdate("DELETE FROM " + SQLIdentifier.DATABASE_NAME_SVG);
				stmt.executeUpdate("DELETE FROM " + SQLIdentifier.DATABASE_NAME_MAIN);
				stmt.executeUpdate("DELETE FROM " + SQLIdentifier.DATABASE_NAME_NORMALIZED);

			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			for (File f : files) {
				idCount++;
				name = f.getName();
				path = f.getAbsolutePath();
				path = FilenameUtils.removeExtension(path);

				File svgFile = new File(path + ".svg");

				if (svgFile.isFile()) {
					try {
						fisSVG = new FileInputStream(svgFile);
						SVGParser svgParser = new SVGParser();
						List<ArrayList<Point2D>> result = svgParser.getPathData(svgFile);
						ArrayList<Point2D> points = result.get(0);
						ArrayList<Point2D> normalizedPoints = result.get(1);

						Double width = svgParser.getWidth();
						Double height = svgParser.getHeight();
						updateSVGDatabase(points, normalizedPoints, width, height, idCount);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					fisContent = new FileInputStream(f);
					// blobContent = FileUtils.readFileToString(f);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					BufferedImage img = ImageIO.read(f);
					updateHammingDatabase(img, Integer.toString(idCount));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					conn.setAutoCommit(false);
					ps = conn.prepareStatement(INSERT_ELEMENT);
					ps.setInt(1, idCount);
					ps.setDate(2, sqlDate);
					ps.setString(3, name);
					ps.setBinaryStream(4, fisContent, (int) f.length());
					ps.setBinaryStream(5, fisSVG, (int) svgFile.length());

					ps.executeUpdate();
					conn.commit();

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						ps.close();
						fisContent.close();
						fisSVG.close();
					} catch (SQLException | IOException e) {
						e.printStackTrace();

					}

				}

				// System.out.println(f.getAbsolutePath());
			}
		} else {
			System.out.println(dir.getAbsolutePath());
		}
	}

	private void updateSVGDatabase(ArrayList<Point2D> points, ArrayList<Point2D> normalizedPoints, Double width,
			Double height, Integer id) {

		String pointsString = "";
		String normalizedPointString = "";
		for (Point2D p : points) {
			pointsString += p.getX() + "," + p.getY() + "|";
		}
		for (Point2D p : normalizedPoints) {
			normalizedPointString += p.getX() + "," + p.getY() + "|";

		}
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(this.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		try {
			conn = myDS.getConnection();

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String INSERT_ELEMENT = "insert into " + SQLIdentifier.DATABASE_NAME_SVG + " (" + SQLIdentifier.SVG_ID + ","
				+ SQLIdentifier.SVG_WIDTH + "," + SQLIdentifier.SVG_HEIGHT + "," + SQLIdentifier.SVG_POINTS + ","
				+ SQLIdentifier.SVG_NORMALIZEDPOINTS + ") values (?,?,?,?,?)";

		java.sql.PreparedStatement ps = null;

		try {
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(INSERT_ELEMENT);
			ps.setInt(1, id);
			ps.setDouble(2, width);
			ps.setDouble(3, height);
			ps.setString(4, pointsString);
			ps.setString(5, normalizedPointString);

			ps.executeUpdate();
			conn.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}

		}
	}
}