package org.example;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SQLtoJSON {

	private static final String REQUEST = "select " + SQLIdentifier.DATABASE_NAME_MAIN + "." + SQLIdentifier.IMAGES_ID
			+ "," + SQLIdentifier.DATABASE_NAME_MAIN + "." + SQLIdentifier.IMAGES_CONTENT + ","
			+ SQLIdentifier.DATABASE_NAME_HAMMING + "." + SQLIdentifier.HAMMING_CONTENT + ","
			+ SQLIdentifier.DATABASE_NAME_SVG + "." + SQLIdentifier.SVG_NORMALIZEDPOINTS + " from "
			+ SQLIdentifier.DATABASE_NAME_MAIN + " LEFT JOIN " + SQLIdentifier.DATABASE_NAME_HAMMING + " ON "
			+ SQLIdentifier.DATABASE_NAME_MAIN + "." + SQLIdentifier.IMAGES_ID + " = "
			+ SQLIdentifier.DATABASE_NAME_HAMMING + "." + SQLIdentifier.HAMMING_ID + " LEFT JOIN "
			+ SQLIdentifier.DATABASE_NAME_SVG + " ON " + SQLIdentifier.DATABASE_NAME_MAIN + "."
			+ SQLIdentifier.IMAGES_ID + " = " + SQLIdentifier.DATABASE_NAME_SVG + "." + SQLIdentifier.SVG_ID;

	private static final String GEO_REQUEST = "select " + SQLIdentifier.GEO_ID + "," + SQLIdentifier.GEO_IDGEO + ","
			+ SQLIdentifier.GEO_IMAGE + "," + SQLIdentifier.GEO_ADRESS + "," + SQLIdentifier.GEO_HOUSE_NUMBER + ","
			+ SQLIdentifier.GEO_BINARY + "," + SQLIdentifier.GEO_NORMALIZEDPOINTS + " from "
			+ SQLIdentifier.DATABASE_NAME_GEO;

	
	public static JSONArray getAllSVG() {
		InitialContext ic;
		DataSource myDS = null;

		JSONArray resultJSON = new JSONArray();
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(SQLIdentifier.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery(REQUEST);
			ResultSetMetaData rsmd = results.getMetaData();
			String typeID = rsmd.getColumnTypeName(1);
			String typeContent = rsmd.getColumnTypeName(2);

			while (results.next()) {
				JSONObject obj = new JSONObject();
				if (typeID == "INT") {
					obj.put(SQLIdentifier.IMAGES_ID, results.getInt(1));
				}
				if (typeContent == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.IMAGES_CONTENT, results.getBytes(2));

				}
				obj.put(SQLIdentifier.PROBABILITY, -1.0);
				resultJSON.put(obj);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultJSON;
	}
	
	public static JSONArray getAllImages() {
		InitialContext ic;
		DataSource myDS = null;

		JSONArray resultJSON = new JSONArray();
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(SQLIdentifier.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery(REQUEST);
			ResultSetMetaData rsmd = results.getMetaData();
			String typeID = rsmd.getColumnTypeName(1);
			String typeContent = rsmd.getColumnTypeName(2);
			String typeHamming = rsmd.getColumnTypeName(3);
			String typeNormalizedPoints = rsmd.getColumnTypeName(4);
			int i = 0;
			while (results.next()) {
				JSONObject obj = new JSONObject();
				if (typeID == "INT") {
					obj.put(SQLIdentifier.IMAGES_ID, results.getInt(1));
				}
				if (typeContent == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.IMAGES_CONTENT, results.getBytes(2));

				}
				if (typeHamming == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.DATABASE_NAME_HAMMING + SQLIdentifier.HAMMING_CONTENT, results.getBytes(3));

				}
				obj.put(SQLIdentifier.SVG_NORMALIZEDPOINTS, results.getString(4));
				obj.put(SQLIdentifier.PROBABILITY, -1.0);

				resultJSON.put(obj);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultJSON;
	}

	public static JSONArray getResultsfromID(JSONArray jsonArray, int count) {
		InitialContext ic;
		DataSource myDS = null;

		JSONArray resultJSON = new JSONArray();
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(SQLIdentifier.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		String SELECT_IMAGES = REQUEST + " order by case " + SQLIdentifier.DATABASE_NAME_MAIN + "."
				+ SQLIdentifier.IMAGES_ID;

		int length = jsonArray.length();
		int limit = Math.min(count, length);
		for (int i = 0; i < limit; i++) {
			try {
				String append = " when " + String.valueOf(jsonArray.getJSONObject(i).getInt(SQLIdentifier.IMAGES_ID))
						+ " then " + String.valueOf(limit - i);
				SELECT_IMAGES += append;
			} catch (JSONException e) {
				e.printStackTrace();
				break;
			}
		}

		SELECT_IMAGES += " end desc limit " + String.valueOf(limit);
		// System.out.println(SELECT_IMAGES);

		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery(SELECT_IMAGES);
			ResultSetMetaData rsmd = results.getMetaData();
			String typeID = rsmd.getColumnTypeName(1);
			String typeContent = rsmd.getColumnTypeName(2);
			String typeHamming = rsmd.getColumnTypeName(3);
			String typeNormalizedPoints = rsmd.getColumnTypeName(4);
			int i = 0;
			while (results.next()) {
				JSONObject obj = new JSONObject();
				if (typeID == "INT") {
					obj.put(SQLIdentifier.IMAGES_ID, results.getInt(1));
				}
				if (typeContent == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.IMAGES_CONTENT, results.getBytes(2));

				}
				if (typeHamming == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.DATABASE_NAME_HAMMING + SQLIdentifier.HAMMING_CONTENT, results.getBytes(3));

				}
				obj.put(SQLIdentifier.SVG_NORMALIZEDPOINTS, results.getString(4));
				obj.put(SQLIdentifier.PROBABILITY, jsonArray.getJSONObject(i).getDouble(SQLIdentifier.PROBABILITY));
				
				if (jsonArray.getJSONObject(i).has("rotatedPoints") && jsonArray.getJSONObject(i).has("rotation")) {
					String normPointString = jsonArray.getJSONObject(i).getString("rotatedPoints");
					obj.put("rotatedInputPoints", normPointString);	
					obj.put("rotation", jsonArray.getJSONObject(i).getInt("rotation") ); 

				} else if (jsonArray.getJSONObject(i).has("rotation")) {
					obj.put("rotation", jsonArray.getJSONObject(i).getInt("rotation") ); 
//					System.out.println("Rotation addedd to result set");
				}
				resultJSON.put(obj);
				i++;
				if (i == 10) {
					break;
				}

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultJSON;

	}

	public static JSONArray getGeoResultsfromID(JSONArray jsonArray, int count) {
		InitialContext ic;
		DataSource myDS = null;

		JSONArray resultJSON = new JSONArray();
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(SQLIdentifier.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		String SELECT_IMAGES = GEO_REQUEST + " order by case " + SQLIdentifier.DATABASE_NAME_GEO + "."
				+ SQLIdentifier.GEO_ID;

		int length = jsonArray.length();
		int limit = Math.min(count, length);
		for (int i = 0; i < limit; i++) {
			try {
				String append = " when " + String.valueOf(jsonArray.getJSONObject(i).getInt(SQLIdentifier.GEO_ID))
						+ " then " + String.valueOf(limit - i);
				SELECT_IMAGES += append;
			} catch (JSONException e) {
				e.printStackTrace();
				break;
			}
		}

		SELECT_IMAGES += " end desc limit " + String.valueOf(limit);
		// System.out.println(SELECT_IMAGES);

		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			ResultSet results = stmt.executeQuery(SELECT_IMAGES);
			ResultSetMetaData rsmd = results.getMetaData();
			String typeID = rsmd.getColumnTypeName(1);
			String typeGeoID = rsmd.getColumnTypeName(2);
			String typeContent = rsmd.getColumnTypeName(3);
			String typeAdress = rsmd.getColumnTypeName(4);
			String typeHouseNumber = rsmd.getColumnTypeName(5);
			String typeHamming = rsmd.getColumnTypeName(6);
			int i = 0;

			while (results.next()) {
				JSONObject obj = new JSONObject();
				if (typeID == "INT") {
					obj.put(SQLIdentifier.GEO_ID, results.getInt(1));
				}
				if (typeGeoID == JDBCType.VARCHAR.getName()) {
					obj.put(SQLIdentifier.GEO_IDGEO, results.getString(2));
				}
				if (typeContent == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.GEO_IMAGE, results.getBytes(3));

				}
				if (typeAdress == JDBCType.VARCHAR.getName()) {
					obj.put(SQLIdentifier.GEO_ADRESS, results.getString(4));
				}
				if (typeHouseNumber == JDBCType.VARCHAR.getName()) {
					obj.put(SQLIdentifier.GEO_HOUSE_NUMBER, results.getString(5));
				}
				if (typeHamming == JDBCType.BLOB.getName()) {

					obj.put(SQLIdentifier.GEO_BINARY, results.getBytes(6));

				}
				
				String normPointString = jsonArray.getJSONObject(i).getString("rotatedPoints");
				obj.put(SQLIdentifier.GEO_NORMALIZEDPOINTS, results.getString(7));
				obj.put("rotatedInputPoints", normPointString);

				
				obj.put("rotation", jsonArray.getJSONObject(i).getInt("rotation") ); 

				obj.put(SQLIdentifier.PROBABILITY, jsonArray.getJSONObject(i).getDouble(SQLIdentifier.PROBABILITY));

				resultJSON.put(obj);
				i++;
				if (i == 10) {
					break;
				}

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultJSON;

	}

}
