package org.example;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.parser.DefaultPointsHandler;
import org.apache.batik.parser.ParseException;
import org.apache.batik.parser.PointsHandler;
import org.apache.batik.parser.PointsParser;
import org.apache.batik.parser.TransformListParser;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGElement;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGMatrix;
import org.w3c.dom.svg.SVGPoint;
import org.w3c.dom.svg.SVGSVGElement;

public class SVGParser {

	private ArrayList<Point2D> points = new ArrayList<Point2D>();
	private ArrayList<Point2D> normalizedPoints = new ArrayList<Point2D>();

	private double width;
	private double height;

	public SVGParser() {

	}

	public List<ArrayList<Point2D>> getPathData(File f) {

		// DocumentBuilderFactory dbFactory =
		// DocumentBuilderFactory.newInstance();
		// DocumentBuilder dBuilder;

		try {
			String parser = XMLResourceDescriptor.getXMLParserClassName();
			SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory(parser);
			Document doc = factory.createSVGDocument(f.toURI().toString());
			Element baseElement = doc.getDocumentElement();

			UserAgent userAgent = new UserAgentAdapter();
			DocumentLoader loader = new DocumentLoader(userAgent);
			BridgeContext ctx = new BridgeContext(userAgent, loader);
			ctx.setDynamicState(BridgeContext.DYNAMIC);
			GVTBuilder builder = new GVTBuilder();
			builder.build(ctx, doc);

			parseSVG(baseElement);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ArrayList<Point2D>> result = new ArrayList<ArrayList<Point2D>>(2);
		result.add(this.points);
		result.add(this.normalizedPoints);
		return result;

	}

	public Double getHeight() {
		return 500.0;
	}

	public Double getWidth() {
		return 500.0;
	}

	private void parseSVG(Element baseElement) {

		PointsParser pp = new PointsParser();
		PointsHandler ph = new DefaultPointsHandler() {
			public void point(float x, float y) throws ParseException {
				Point2D p = new Point2D.Float(x, y);
				points.add(p);

			}
		};
		pp.setPointsHandler(ph);
		NodeList nL = baseElement.getElementsByTagName("polyline");
		for (int i = 0; i < nL.getLength(); i++) {
			Element e = (Element) nL.item(i);
			String pointAttribute = e.getAttribute("points");
			pp.parse(pointAttribute);
		}

		double minX = 0, maxX = 0, minY = 0, maxY = 0;

		for (Point2D p : points) {
			if (p.getX() > maxX) {
				maxX = p.getX();
			} else if (p.getX() < minX) {
				minX = p.getX();
			}
			if (p.getY() > maxY) {
				maxY = p.getY();
			} else if (p.getY() < minY) {
				minY = p.getY();
			}
		}
		baseElement.setAttribute("viewBox",
				String.valueOf(Math.floor(minX)) + " " + String.valueOf(Math.floor(minY)) + " "
						+ String.valueOf(Math.ceil(maxX) - Math.floor(minX)) + " "
						+ String.valueOf(Math.ceil(maxY) - Math.floor(minY)));
//		System.out.println(String.valueOf(Math.floor(minX)) + " " + String.valueOf(Math.floor(minY)) + " "
//				+ String.valueOf(Math.ceil(maxX) - Math.floor(minX)) + " "
//				+ String.valueOf(Math.ceil(maxY) - Math.floor(minY)));
		baseElement.setAttribute("preserveAspectRatio", "xMidYMid");
		baseElement.setAttribute("width", String.valueOf(Math.ceil(this.getWidth())) + "px");
		baseElement.setAttribute("height", String.valueOf(Math.ceil(this.getHeight())) + "px");

		if (nL.getLength() > 0) {
			Element e = (Element) nL.item(0);

			if ((baseElement instanceof SVGLocatable) && (e instanceof SVGElement)) {
				SVGSVGElement docSVGElement = (SVGSVGElement) baseElement;

				SVGLocatable locatable = (SVGLocatable) baseElement;
				SVGElement svgPolyline = (SVGElement) e;
				SVGMatrix transformationMatrix = docSVGElement.createSVGMatrix();

				SVGLocatable locatablePolyline = (SVGLocatable) svgPolyline;

				transformationMatrix = locatablePolyline.getScreenCTM();

				for (Point2D p : this.points) {
					SVGPoint svgPoint = docSVGElement.createSVGPoint();
					svgPoint.setX((float) p.getX());
					svgPoint.setY((float) p.getY());
					SVGPoint svgPoint1 = svgPoint.matrixTransform(transformationMatrix);
					normalizedPoints.add(new Point2D.Float(svgPoint1.getX(), svgPoint1.getY()));
					// System.out.print(svgPoint1.getX() + "|" +
					// svgPoint1.getY() + ",");

				}
				// System.out.println("");

			}
		}

		//
		// TransformListParser parser = new TransformListParser();
		// JSVGCanvas canvas = new JSVGCanvas();
	}

}
