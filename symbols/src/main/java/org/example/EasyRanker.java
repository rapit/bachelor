package org.example;

import java.awt.geom.Point2D;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instances;

public class EasyRanker extends Ranker {
	public JSONArray rank(double[] fDistribution, Classifier classifier, Instances dataSet) {
		
		List<JSONObject> jsonValues = new ArrayList<JSONObject>();
		JSONArray sortedResultJson = new JSONArray();
		
		System.out.println(classifier.toString());

		for (int i = 0; i < fDistribution.length; i++) {
			String className = dataSet.classAttribute().value(i);
			double predictionProbability = fDistribution[i];

			JSONObject obj = new JSONObject();
			int classID = 999;
			try {
				classID = Integer.parseInt(className);
			} catch (NumberFormatException e) {
				
			}
			obj.put(SQLIdentifier.IMAGES_ID, classID);
			obj.put(this.cNameProb, predictionProbability * 100);
			jsonValues.add(obj);


		}
		
		
		
		Collections.sort(jsonValues, this.probCompare);
		
		
		for (int i = 0; i < Math.min(10, jsonValues.size()); i++) { // jsonValues.size()
			sortedResultJson.put(jsonValues.get(i));
		}
		return sortedResultJson;

	}

	public JSONArray rankArray(int[][] results) {
		List<JSONObject> jsonValues = new ArrayList<JSONObject>();
		JSONArray sortedResultJson = new JSONArray();
	

		for (int i = 0; i < results.length; i++) {
			int classID = results[i][0];
			double predictionProbability = (double) results[i][1];

			JSONObject obj = new JSONObject();
			obj.put(SQLIdentifier.IMAGES_ID, classID);
			obj.put(this.cNameProb, predictionProbability);
			jsonValues.add(obj);
		}
		
		
		
//		Collections.sort(jsonValues, this.probCompare);
		
		
		for (int i = 0; i < Math.min(10, jsonValues.size()); i++) { // jsonValues.size()
			sortedResultJson.put(jsonValues.get(i));
		}
		return sortedResultJson;
	}

}
