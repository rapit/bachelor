package org.example;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.io.IOException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.example.ClassificationServlet;
import org.example.TrainingServlet;

import java.util.Properties;

import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebXmlConfiguration;
import org.eclipse.jetty.util.component.Container;


public class JettyMain extends AbstractHandler
{

    public static void main(String[] args) throws Exception
    {

        
        Server server = new Server(8080);
       
        
        final WebAppContext root = new WebAppContext();
//        System.out.println(root.getContextPath());
        root.setContextPath("/");
//        System.out.println("new: " + root.getContextPath());

        root.setResourceBase("./src/main/webapp/");

//        root.setResourceBase("./symbols/src/main/webapp/"); // for packing

        EnvConfiguration envConfiguration = new EnvConfiguration();
        envConfiguration.setJettyEnvXml(ClassificationServlet.class.getResource("/WEB-INF/jetty-env.xml").toURI().toURL());
//        envConfiguration.setJettyEnvXml(UpdateServlet.class.getResource("/WEB-INF/jetty-env.xml").toURI().toURL());

        root.setConfigurations(new Configuration[]{envConfiguration, new PlusConfiguration(), new WebXmlConfiguration()});
        root.setDescriptor(ClassificationServlet.class.getResource("/WEB-INF/web.xml").toString());
      
        String test = "haha";
	       root.setAttribute("blabla", test);
        
        ServletHolder helloServletHolder = new ServletHolder(new ClassificationServlet());
        helloServletHolder.setInitParameter("my_key", "my_value");
        ServletHolder trainingServletHolder = new ServletHolder(new TrainingServlet());
        
        root.addServlet(helloServletHolder,"/classify");
        root.addServlet(UpdateServlet.class, "/update");
        root.addServlet(trainingServletHolder, "/addTrainingElement");
        
        server.setHandler(root);

        server.start();
        server.join();
    }

	public void handle(String arg0, Request arg1, HttpServletRequest arg2, HttpServletResponse arg3)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		
	}
}