package org.example;

import java.awt.geom.Point2D;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class Ranker {

	protected Strategy strategy;
	protected String cNameID;
	protected String cNameContent;
	protected String cNameProb;
	protected String cNameRotation = "rotation";


	protected Comparator<JSONObject> probCompare = new Comparator<JSONObject>() {

		private static final String KEY_NAME = "prob";

		@Override
		public int compare(JSONObject a, JSONObject b) {
			Double valA = null;
			Double valB = null;

			try {
				valA = (double) a.get(KEY_NAME);
				valB = (double) b.get(KEY_NAME);
			} catch (JSONException e) {
				System.out.println("Error parsing JSON");
			}

			return -valA.compareTo(valB);
		}
	};

	public Ranker(Strategy s, String idName, String contentName, String probName) {
		this.strategy = s;
		this.cNameContent = contentName;
		this.cNameID = idName;
		this.cNameProb = probName;
	}

	public Ranker() {
		this.strategy = Strategy.GREEDY;
		this.cNameContent = SQLIdentifier.HAMMING_CONTENT;
		this.cNameID = SQLIdentifier.HAMMING_ID;
		this.cNameProb = "prob";
		
	}

}
