package org.example.util;

import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.util.AffineTransformation;

public class Rasterizer {

	private static final int SIZE = 500;
	private static final int DEFAULT_FACTOR = 20;
	
	public static List<LineString> normalizeLineString(List<LineString> lss) {
		GeometryFactory factory = new GeometryFactory();
		LineString[] lineStringArray = new LineString[lss.size()];
		for (int i = 0; i < lineStringArray.length; i++) {
			lineStringArray[i] = lss.get(i);
		}
		MultiLineString combined = factory.createMultiLineString(lineStringArray);

		Envelope view = combined.getEnvelopeInternal();
		double scale = 1.0;
		
		if (view.getWidth() > view.getHeight()) {
			scale = SIZE / view.getWidth();
		} else {
			scale = SIZE / view.getHeight();

		}
	    double xOffset = - view.getMinX() * scale + ((SIZE - view.getWidth() * scale) / 2);
	    double yOffset = - view.getMinY() * scale + ((SIZE - view.getHeight() * scale) / 2);
	    
//	    System.out.println("XOffset: "+ xOffset +  " | yoffset: " + yOffset);
	    
	    
	    AffineTransformation at = new AffineTransformation( scale, 0.0, xOffset, 0.0, scale, yOffset );
	    for (LineString ls : lss) {
		    ls.apply(at);	
	    }
//		System.out.println("Amount before call: " + combined.getNumPoints());

	    return lss;
	}

	public static String rasterize(List<LineString> lineStrings, int factor) {
		lineStrings = Rasterizer.normalizeLineString(lineStrings);
//		line = Rasterizer.cutSamePoints(line, 30);
		
		if (SIZE % factor != 0) {
			factor = DEFAULT_FACTOR;
		}
		int gridLength = SIZE / factor;
		int gridAmount = factor;

		boolean[][] raster = new boolean[gridAmount][gridAmount];
		
		for (int i = 0; i < raster.length; i++) {
			for (int j = 0; j < raster.length; j++) {
				raster[i][j] = false;
			}
		}

		GeometryFactory geometryFactory = new GeometryFactory();

		for (int i = 0; i < SIZE; i += gridLength) {
			for (int j = 0; j < SIZE; j += gridLength) {
				Coordinate[] coords = new Coordinate[5];
				coords[0] = new Coordinate(i,j);
				coords[1] = new Coordinate(i+gridLength,j);
				coords[2] = new Coordinate(i+gridLength,j+gridLength);
				coords[3] = new Coordinate(i,j+gridLength);
				coords[4] = new Coordinate(i,j);
				Polygon polygonFromCoordinates = geometryFactory.createPolygon(coords);
				for (LineString ls : lineStrings) {
					if (polygonFromCoordinates.intersects(ls)) {
						raster[(SIZE / gridLength) - 1 - (j / gridLength)][(i / gridLength)] = true;
						break;
					}
				}


			}
		}
		
		String resultString = "";
		for (int i = 0; i < raster.length; i++) {
			for (int j = 0; j < raster.length; j++) {
				if (raster[i][j]) {
					resultString += "1";
				} else {
					resultString += "0";
				}
			}
		}
		return resultString;
	}

	public static LineString cutSamePoints(LineString line, int epsilon) {
		Coordinate[] coords = line.getCoordinates();
//		System.out.println("Amount before: " + coords.length + "/n");
//		for (int i = 0; i < coords.length; i++) {
//			System.out.print("(" + coords[i].x + "|" + coords[i].y + ")");
//		}

		int nullCounter = 0;
		for (int i = 0; i < coords.length - 1; i++) {
			if (coords[i+1].distance(coords[i]) <= epsilon) {
				coords[i] = null;
				nullCounter++;
			
			}
		}
		Coordinate[] newCoords = new Coordinate[nullCounter];
		
		int j = 0;
		for (int i = 0; i < coords.length; i++) {
			if (coords[i] != null) {
				newCoords[j] = coords[i] ;
//				System.out.println(j + ": " + newCoords[j].x + "|" +  newCoords[j].y );
				j++;
			}
		}
		GeometryFactory factory = new GeometryFactory();
		LineString newLineString = factory.createLineString(newCoords);
//		System.out.println("Amount after: " + newCoords.length);

		return newLineString;
	}

}
