package org.example.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.example.Preprocessor;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.util.AffineTransformation;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

public class Curvature {
	private static final int dMin = 10;
	private static final int dMinSq = dMin * dMin;
	private static final int dMax = 2 * dMin;
	private static final int dMaxSq = dMax * dMax;
	private static final double cosMin = -0.75;
	private static final double screenSize = 500;

	private static LineString line;

	private static enum sharp {
		EMPTY, FAIL_UPPER, FAIL_LOWER, FAIL, OK
	};

	public static String calculateCurvaturePoints(List<LineString> lss) {

		String resultString = "[";
		lss = Rasterizer.normalizeLineString(lss);
		for (LineString lineStrings : lss) {
//			System.out.println("Symbol: ");
//			for (int i = 0; i < lineStrings.getNumPoints(); i++) {
//				Point p = lineStrings.getPointN(i);
//				System.out.print(",(" + (int) p.getX() + "," + (int) p.getY() + ")");
//			}
//			System.out.println();
//			System.out.println("----------------------------------------------------------");
//			System.out.println();

			line = (LineString) DouglasPeuckerSimplifier.simplify(lineStrings, 20);

//			System.out.println("Symbol after Douglas Peucker: ");
//			for (int i = 0; i < line.getNumPoints(); i++) {
//				Point p = line.getPointN(i);
//				System.out.print(",(" + (int) p.getX() + "," + (int) p.getY() + ")");
//			}
//			System.out.println();
//			System.out.println("----------------------------------------------------------");
//			
			ArrayList<Point> curvaturePoints = new ArrayList<Point>();
			for (int i = 0; i < line.getNumPoints(); i++) {
				curvaturePoints.add(line.getPointN(i));
			}
			
//			int length = line.getNumPoints();
//			double sharpness[][] = new double[length][3];
//			sharp[] angle = new sharp[length];
//			double maxCos;
//			double aSq, bSq, cSq;
//			boolean seenOneUpper;
//
//			for (int i = 0; i < length; i++) {
//				maxCos = -1;
//				seenOneUpper = false;
//				sharpness[i][0] = -1.0;
//				sharpness[i][1] = -1.0;
//				sharpness[i][2] = 1000.0;
//				angle[i] = sharp.EMPTY;
//				for (int j = i + 1; j < length; j++) {
//					aSq = squareDistance(i, j);
//					if (seenOneUpper && aSq > dMaxSq) {
//						if (angle[i] == sharp.EMPTY) {
//							angle[i] = sharp.FAIL_UPPER;
//						}
//						break;
//					}
//					if (aSq < dMinSq) {
//						continue;
//					}
//					seenOneUpper = true;
//					boolean seenOneLower = false;
//
//					for (int k = i - 1; k >= 0; k--) {
//						bSq = squareDistance(i, k);
//						if (seenOneLower && bSq > dMaxSq) {
//							if (angle[i] == sharp.EMPTY) {
//								angle[i] = sharp.FAIL_LOWER;
//							}
//							break;
//						}
//						if (bSq < dMinSq) {
//							continue;
//						}
//						seenOneLower = true;
//						cSq = squareDistance(j, k);
//						double top = aSq + bSq - cSq;
//						double bottom = 2.0 * Math.sqrt(aSq * bSq);
//						double cos = -2;
//						if (bottom != 0.0) {
//							cos = top / bottom;
//						}
//						if (cos < cosMin) {
//							if (angle[i] == sharp.EMPTY) {
//								sharpness[i][2] = cos;
//								angle[i] = sharp.FAIL;
//							}
//							break;
//						}
//						if (maxCos < cos) {
//							maxCos = cos;
//							sharpness[i][0] = j;
//							sharpness[i][1] = k;
//							sharpness[i][2] = cos;
//							angle[i] = sharp.OK;
//						} else {
//							if (angle[i] == sharp.EMPTY) {
//								sharpness[i][2] = cos;
//								angle[i] = sharp.FAIL;
//							}
//						}
//					}
//				}
//			}
//
//			ArrayList<Point> curvaturePoints = new ArrayList<Point>();
//			int lastFound = -1;
//			for (int i = 0; i < length; i++) {
//				boolean found = false;
//				if (sharpness[i][2] > cosMin && angle[i] == sharp.OK) {
//					if (lastFound >= 0) {
//						double sqDist = squareDistance(lastFound, i);
//						if (sqDist > dMaxSq) {
//							found = true;
//						} else if (sharpness[i][2] > sharpness[lastFound][2]) {
//							found = true;
//						}
//					} else {
//						found = true;
//					}
//				}
//				if (found) {
//					lastFound = i;
//					curvaturePoints.add(line.getPointN(i));
//				}
//			}
//
//			curvaturePoints.add(line.getStartPoint());
//			curvaturePoints.add(line.getEndPoint());

//			Preprocessor.cutSameJTSPoint(curvaturePoints, 30.0);

			String featurePoints = "";
			// System.out.println();
			for (Point p : curvaturePoints) {
				// System.out.print("(" + p.getX() + "," + p.getY() + "),");
				featurePoints += "[" + p.getX() + "," + p.getY() + "],";
			}
			resultString += featurePoints.substring(0, featurePoints.length());
		}

		return resultString + "]";
	}

	private static double squareDistance(int i, int j) {
		int length = line.getNumPoints();
		if (i > 0 && i < length && j > 0 && j < length) {
			Coordinate c1 = line.getCoordinateN(i);
			Coordinate c2 = line.getCoordinateN(j);
			double xDiff = c2.x - c1.x;
			double yDiff = c2.y - c1.y;
			return (xDiff * xDiff + yDiff * yDiff);
		}
		return 0.0;
	}

	public static String calculateStartEndPoints(List<LineString> lineStrings) {
		String startEndPoints = "[";

		for (LineString ls : lineStrings) {
			startEndPoints += "[" + ls.getStartPoint().getX() + "," + ls.getStartPoint().getY() + "] , ["
					+ ls.getEndPoint().getX() + "," + ls.getEndPoint().getY() + "] ,";
		}
		return startEndPoints.substring(0, startEndPoints.length()) + "]";
	}
}
