package org.example.util;

import java.awt.geom.Point2D;

/*******************************************************************************
 * This software is provided as a supplement to the authors' textbooks on
 * digital image processing published by Springer-Verlag in various languages
 * and editions. Permission to use and distribute this software is granted under
 * the BSD 2-Clause "Simplified" License (see
 * http://opensource.org/licenses/BSD-2-Clause). Copyright (c) 2006-2016 Wilhelm
 * Burger, Mark J. Burge. All rights reserved. Visit http://imagingbook.com for
 * additional details.
 *******************************************************************************/

public class ImageMoments {
	static final double EPS = 0.1E-4;
	static final int BACKGROUND = 0;

	public static double moment(BitMatrix I, int p, int q) {
		double Mpq = 0.0;
		for (int v = 0; v < I.getHeight(); v++) {
			for (int u = 0; u < I.getWidth(); u++) {
				if (I.get(u, v) != false) {
					Mpq += Math.pow(u, p) * Math.pow(v, q);
				}
			}
		}
		return Mpq;
	}

	public static double centralMoment(BitMatrix I, int p, int q) {
		double m00 = moment(I, 0, 0); // region area
		double xCtr = moment(I, 1, 0) / m00;
		double yCtr = moment(I, 0, 1) / m00;
		double cMpq = 0.0;
		for (int v = 0; v < I.getHeight(); v++) {
			for (int u = 0; u < I.getWidth(); u++) {
				if (I.get(u, v) != false) {
					cMpq += Math.pow(u - xCtr, p) * Math.pow(v - yCtr, q);
				}
			}
		}
		return cMpq;
	}

	public static double normalCentralMoment(BitMatrix I, int p, int q) {
		double m00 = moment(I, 0, 0);
		double norm = Math.pow(m00, 0.5 * (p + q + 2));
		return centralMoment(I, p, q) / norm;
	}

	public static double calcOrientation(String binary) {
		int length = binary.length();
		int sqlength = (int) Math.sqrt(length);
		String hammingString = "";

		for (int i = 0; i < binary.length(); i++) {
			if ((i > 0) && (i % sqlength) == 0) {
				hammingString += "s";
			}
			hammingString += binary.charAt(i);

		}
		// System.out.println(hammingString);
		double orientation = 0.0;

		try {
			BitMatrix bMatrix = BitMatrix.parse(hammingString, "1", "0");

			double u11 = centralMoment(bMatrix, 1, 1);
			double u20 = centralMoment(bMatrix, 2, 0);
			double u02 = centralMoment(bMatrix, 0, 2);

			if (!(Math.abs(u20 - u02) < EPS)) {
				orientation = Math.toDegrees(0.5 * Math.atan2(u20 - u02, 2 * u11)) - 45;
			}
		} catch (IllegalArgumentException e) {
			System.err.print("No valid binary String");
		}

		return orientation;
	}

	public static Point2D calcGravity(String binary) {
		int length = binary.length();
		int sqlength = (int) Math.sqrt(length);
		String hammingString = "";

		for (int i = 0; i < binary.length(); i++) {
			if ((i > 0) && (i % sqlength) == 0) {
				hammingString += "s";
			}
			hammingString += binary.charAt(i);

		}
		double xCtr = 250;
		double yCtr = 250;
		
		try {
			BitMatrix bMatrix = BitMatrix.parse(hammingString, "1", "0");
			double m00 = moment(bMatrix, 0, 0); // region area
			xCtr = moment(bMatrix, 1, 0) / m00;
			yCtr = moment(bMatrix, 0, 1) / m00;

		} catch (IllegalArgumentException e) {
			System.err.print("No valid binary String");
		}
		return new Point2D.Double(xCtr, yCtr);
	}

}
