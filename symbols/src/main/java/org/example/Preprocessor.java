package org.example;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.util.AffineTransformation;

import math.geom2d.Vector2D;

public class Preprocessor {
	
	public static final int WINDOW_SIZE = 500;

	public static ImageScaleInstance getScaledCurvaturePointsFromString(String inputFeaturePoints, int windowSize) {
		ArrayList<Point2D> featurePoints = new ArrayList<Point2D>();

		JSONArray parsePoints = new JSONArray(inputFeaturePoints);

		double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE, maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;

		for (int i = 0; i < parsePoints.length(); i++) {
			Point2D point = new Point2D.Double(parsePoints.getJSONArray(i).getDouble(0),
					parsePoints.getJSONArray(i).getDouble(1));
			featurePoints.add(point);
			if (point.getX() < minX) {
				minX = point.getX();
			}
			if (point.getX() > maxX) {
				maxX = point.getX();
			}
			if (point.getY() < minY) {
				minY = point.getY();
			}
			if (point.getY() > maxY) {
				maxY = point.getY();
			}
		}

		double xDelta = maxX - minX;
		double yDelta = maxY - minY;
		double scaleFactor;
		int xOffset = 0, yOffset = 0;

		if (xDelta > yDelta) {
			if (xDelta == 0) {
				scaleFactor = 1;
			} else {
				scaleFactor = windowSize / xDelta;

			}
			yOffset = (int) (windowSize - (yDelta * scaleFactor)) / 2;

		} else {
			if (yDelta == 0) {
				scaleFactor = 1;
			} else {
				scaleFactor = windowSize / yDelta;

			}
			xOffset = (int) (windowSize - (xDelta * scaleFactor)) / 2;

		}
		System.out.println("");
		for (Point2D p : featurePoints) {
			double x = (p.getX() - minX) * scaleFactor + xOffset;
			double y = (p.getY() - minY) * scaleFactor + yOffset;
			p.setLocation(x, y);
			System.out.print("(" + (int) x + "," + (int) y + ")" + ",");
		}
		featurePoints = cutSamePoints(featurePoints, 10.0);

		double length = 0.0;
		for (int i = 0; i < featurePoints.size() - 1; i++) {
			length += featurePoints.get(i).distance(featurePoints.get(i + 1));
		}

		ImageScaleInstance isi = new ImageScaleInstance(featurePoints, minX, minY, maxX, maxY, scaleFactor, xOffset,
				yOffset, length);

		return isi;
	}

	public static ArrayList<Point2D> cutSamePoints(ArrayList<Point2D> fP, double epsilon) {

		for (int i = 0; i < fP.size() - 1; i++) {
			if (fP.get(i).distance(fP.get(i + 1)) < epsilon) {
				fP.remove(i + 1);
				i--;
			}

		}
		return fP;

	}
	
	public static ArrayList<com.vividsolutions.jts.geom.Point> cutSameJTSPoint(ArrayList<com.vividsolutions.jts.geom.Point> fP, double epsilon) {

		for (int i = 0; i < fP.size() - 1; i++) {
			if (fP.get(i).distance(fP.get(i + 1)) < epsilon) {
				fP.remove(i + 1);
				i--;
			}

		}
		return fP;

	}


	public static int countHammingString(String hamming) {
		int count = 0;
		for (int i = 0; i < hamming.length(); i++) {
			if (hamming.charAt(i) == '1') {
				count++;
			}
		}
		return count;
	}

	public static double degreeSumBetweenPoints(ArrayList<Point2D> featurePoints) {
		double degreeSum = 0.0;
		for (int i = 0; i < featurePoints.size() - 2; i++) {
			Vector2D vec1 = new Vector2D(featurePoints.get(i + 1).getX() - featurePoints.get(i).getX(),
					featurePoints.get(i + 1).getY() - featurePoints.get(i).getY());
			Vector2D vec2 = new Vector2D(featurePoints.get(i + 2).getX() - featurePoints.get(i + 1).getX(),
					featurePoints.get(i + 2).getY() - featurePoints.get(i + 1).getY());

			double dotProduct = vec1.normalize().dot(vec2.normalize());
			double deg = Math.acos(dotProduct);
			degreeSum += deg;
		}
		if (Double.isNaN(degreeSum)) {
			return -1.0;
		}
		return degreeSum;
	}

	public static double calculateAveragePointDistance(ArrayList<Point2D> points) {
		int pointSize = points.size();

		if (pointSize < 2) {
			return 0.0;
		}

		double[][] distanceMatrix = new double[pointSize][pointSize];
		double[] minDistance = new double[pointSize];

		for (int i = 0; i < pointSize; i++) {
			for (int j = 0; j < pointSize; j++) {
				distanceMatrix[i][j] = -1.0;
			}
			distanceMatrix[i][i] = 0.0;
			minDistance[i] = 0;
		}

		for (int i = 0; i < pointSize; i++) {
			for (int j = 0; j < i; j++) {
				double distance = points.get(i).distance(points.get(j));
				distanceMatrix[i][j] = distance;
				distanceMatrix[j][i] = distance;
			}
		}
		for (int i = 0; i < pointSize; i++) {
			int minIndex = 0;
			if (i == 0) {
				minIndex = 1;
			}
			for (int j = 0; j < pointSize; j++) {
				if (distanceMatrix[i][j] > 0.0 && distanceMatrix[i][j] < distanceMatrix[i][minIndex]) {
					minIndex = j;
				}
			}
			minDistance[i] = distanceMatrix[i][minIndex];

		}

		double sum = 0.0;
		for (int i = 0; i < pointSize; i++) {
			sum += minDistance[i];
		}
		// System.out.println("length: " + minDistance.length + " | sum: " +
		// sum);
		sum /= pointSize;
		return sum;

	}

	public static double calcRasterProp(String hamming) {
		int count = 0;
		int length = hamming.length();
		int sqLength = (int) Math.round(Math.sqrt(length));
		int minX = sqLength;
		int maxX = 0;
		int minY = sqLength;
		int maxY = 0;

		for (int i = 0; i < sqLength; i++) {
			for (int j = 0; j < sqLength; j++) {
				if (hamming.charAt(i * sqLength + j) == '1') {
					count++;
					if (j < minX) {
						minX = j;
					}
					if (j > maxX) {
						maxX = j;
					}
					if (i < minY) {
						minY = i;
					}
					if (i > maxY) {
						maxY = i;
					}

				}
			}
		}
		if (maxX >= minX && maxY >= minY) {
			return count / (double) ((maxX - minX + 1) * (maxY - minY + 1));
		}
		return 1.0;
	}

	public static int getSegmentAmount(String startendPoints) {
		int length = 1;
		JSONArray parsePoints = new JSONArray(startendPoints);
		length = parsePoints.length() / 2;
		return length;
	}

	public static double getRotation(String binary) {
		return org.example.util.ImageMoments.calcOrientation(binary);
	}

	public static Point2D getGravityCenter(String binary) {
		return org.example.util.ImageMoments.calcGravity(binary);
	}

	public static String getCurveRaster(ArrayList<Point2D> featurePoints, int windowSize) {
		String binary = "";
		int factor = 50;

		int[][] raster = new int[windowSize / factor][windowSize / factor];

		for (int i = 0; i < raster.length; i++) {
			for (int j = 0; j < raster[0].length; j++) {
				raster[i][j] = 0;
			}
		}

		for (Point2D p : featurePoints) {
			int xElement = Math.min((windowSize / factor)-1,
					(int) Math.round((windowSize / factor) * (p.getX() / (double) windowSize)));
			int yElement = Math.min((windowSize / factor)-1,
					(int) Math.round((windowSize / factor) * (p.getY() / (double) windowSize)));
			if (raster[xElement][yElement] < 5) {
				raster[xElement][yElement] += 1;
			}
		}
		
		for (int i = 0; i < raster.length; i++) {
			for (int j = 0; j < raster[0].length; j++) {
				binary += raster[i][j] + "";
			}
		}

		return binary;

	}

	public static List<LineString> rotateLineStrings(List<LineString> lineStrings, double rotation) {
		LineString[] lineStringArray = new LineString[lineStrings.size()];
		int i = 0;
		for (LineString ls : lineStrings) {
			lineStringArray[i] = ls;
			i++;
		}		
		GeometryFactory factory = new GeometryFactory();
		MultiLineString mls = factory.createMultiLineString(lineStringArray);
		AffineTransformation transformation = new AffineTransformation();
		transformation.rotate(Math.toRadians(-rotation));	
		
		MultiLineString rotatedMultiLineString = (MultiLineString) transformation.transform(mls);
	
		List<LineString> rotatedLineStrings = Preprocessor.scaleAndTranslate(rotatedMultiLineString);


		return rotatedLineStrings;
	}

	private static List<LineString> scaleAndTranslate(MultiLineString mls) {
		double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE, maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;

		List<LineString> rotatedLineStrings = new ArrayList<LineString>();
		
		for (int c = 0; c < mls.getNumGeometries(); c++) {
			LineString ls = (LineString) mls.getGeometryN(c);
			for (int i = 0; i < ls.getNumPoints(); i++) {
				Point2D point = new Point2D.Double(ls.getCoordinateN(i).x, ls.getCoordinateN(i).y);
				if (point.getX() < minX) {
					minX = point.getX();
				}
				if (point.getX() > maxX) {
					maxX = point.getX();
				}
				if (point.getY() < minY) {
					minY = point.getY();
				}
				if (point.getY() > maxY) {
					maxY = point.getY();
				}
			}

		}

		double xDelta = maxX - minX;
		double yDelta = maxY - minY;
		double scaleFactor;

		int xOffset = 0, yOffset = 0;

		if (xDelta > yDelta) {
			if (xDelta == 0) {
				scaleFactor = 1;
			} else {
				scaleFactor = WINDOW_SIZE / xDelta;

			}
			yOffset = (int) (WINDOW_SIZE - (yDelta * scaleFactor)) / 2;

		} else {
			if (yDelta == 0) {
				scaleFactor = 1;
			} else {
				scaleFactor = WINDOW_SIZE / yDelta;

			}
			xOffset = (int) (WINDOW_SIZE - (xDelta * scaleFactor)) / 2;

		}
		
		GeometryFactory factory = new GeometryFactory();
		System.out.println("rotated:");

		System.out.println();

		for (int c = 0; c < mls.getNumGeometries(); c++) {
			LineString ls = (LineString) mls.getGeometryN(c);
			
			Coordinate[] coords = ls.getCoordinates();
			for (int i = 0; i < coords.length; i++) {
				
				double x = (coords[i].x - minX) * scaleFactor + xOffset;
				double y = (coords[i].y - minY) * scaleFactor + yOffset;
				coords[i].setCoordinate(new Coordinate(x,y));
				System.out.print("(" + (int) x + "," + (int) y + ")" + ",");

			}
			LineString rotatedLineString = factory.createLineString(coords);
			rotatedLineStrings.add(rotatedLineString);
		}
		System.out.println();

		

		return rotatedLineStrings;
	}
}
