package org.example;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.lang.model.element.Element;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;


import org.example.util.Curvature;
import org.example.util.Rasterizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

import weka.classifiers.Classifier;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemoveWithValues;

public class ClassificationServlet extends WebSocketServlet {

	private Instances dataSet;
	private Instances rasterDataSet;
	private Instances localDataset;
	private Classifier classifier;
	private Classifier rasterClassifier;
	private Classifier localClassifier;
	private double[] weightVector = { 0.1, 0.3, 0.1, 0.2, 0.3 };
	
	// weight distribution
	// 1. Raster Ranking
	// 2. Point-Pattern Matching
	// 3. WEKA Raster
	// 4. WEKA Global
	// 5. WEKA All Combined


	private static final int COMBINE_OPTION = 0; // 0 is averaging, 1 is maximum

	private Comparator<JSONObject> probCompare = new Comparator<JSONObject>() {

		private static final String KEY_NAME = "prob";

		@Override
		public int compare(JSONObject a, JSONObject b) {
			Double valA = null;
			Double valB = null;

			try {
				valA = (double) a.get(KEY_NAME);
				valB = (double) b.get(KEY_NAME);
			} catch (JSONException e) {
				System.out.println("Error parsing JSON");
			}

			return -valA.compareTo(valB);
		}
	};

	public ClassificationServlet() {
//		try {
////			ServletConfig cfg = getServletConfig();
////			System.out.println(cfg);
////			init();
////			super.init();
//			super.init();
//			
//		} catch (ServletException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		// System.out.println(this.getServletContext().getAttribute("blabla"));
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);
	}
	
//	@Override
//	public void init() throws ServletException {
//		super.init();
//		ServletContext sc = this.getServletContext();
//		System.out.println(sc + " | " + this.getServletConfig());
//
//	}
	
	@Override
	public void configure(WebSocketServletFactory factory) {
		factory.getPolicy().setIdleTimeout(10000);
		
		factory.register(ClassificationSocket.class);		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setHeader("Content-Type", "text/plain");
		response.setHeader("success", "yes");
		PrintWriter writer = response.getWriter();
		writer.write("session=" + request.getSession(true).getId());
		writer.close();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(
				"   doPost called with URI: " + request.getRequestURI() + " and length: " + request.getContentLength());

		StringBuffer jb = new StringBuffer();
		String line = null;

		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);

			}
		} catch (IOException | IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jb.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println(jsonObject);

		response.setHeader("Content-Type", "text/plain");
		response.setHeader("success", "yes");

		JSONArray resultJson = this.lookup(jsonObject);

		try {
			resultJson.write(response.getWriter());
		} catch (IOException e) {

		}

	}

	/**
	 * main classification function
	 * @param jsonObject object transfered by http
	 * @return Array List with sorted result objects
	 * 
	 * possible modes are:
	 * 
	 * -1: training mode, add object to database
	 * 0: framework classification accessible from localhost:8080/classify
	 * 1: geo datat classification accessible from localhost:8080/geo
	 * 2: requests from GeoViewer
	 * 
	 * possible methods are:
	 * 0: Raster Classifier
	 * 1: Point-Pattern-Matching
	 * 2: WEKA classifier with global attributes
	 * 3: WEKA classifier with only local attributes
	 * 4: recursive WEKA (unstable)
	 * 5: combined WEKA (probably the best choice for machine learning classification
	 * 6: all methods combined with weight vector editable above
	 */
	private JSONArray lookup(JSONObject jsonObject) {
		JSONArray resultJson = null;
		int mode = jsonObject.getInt("mode");
		if (mode == 0) {
			int method = jsonObject.getInt("method");
			switch (method) {
			case 0: {
				resultJson = this.classifyRaster(jsonObject, false);
				break;
			}
			case 1: {
				resultJson = this.classifyPoint(jsonObject, false);
				break;
			}
			case 2: {
				resultJson = this.classifyWekaGlobal(jsonObject, false);
				break;
			}
			case 3: {
				resultJson = this.classifyWekaRaster(jsonObject, false);
				break;
			}
			case 4: {
				resultJson = this.classifyRanked(jsonObject);
				break;
			}
			case 5: {
				resultJson = this.classifyWekaCombined(jsonObject, false);
				break;
			}
			case 6: {
				resultJson = this.classifyAllCombined(jsonObject, true);
				break;
			}

			case -1: {

				JSONObject obj = getInformationObject(jsonObject);
				resultJson = SQLtoJSON.getAllImages();

				resultJson.put(obj);
				break;
			}
			default:
				break;
			}
		} else if (mode == 1) {
			resultJson = this.classifyGeo(jsonObject);

		} else if (mode == 2) {
			resultJson = this.classifyGeoViewerRequest(jsonObject);
		}
		return resultJson;

	}

	private JSONArray classifyAllCombined(JSONObject jsonObject, boolean b) {
		JSONArray[] singleResult = new JSONArray[5];
		JSONArray resultJson = null;


		List<JSONObject> allCombined = new ArrayList<JSONObject>();

		singleResult[0] = this.classifyRaster(jsonObject, b);
		singleResult[1] = this.classifyPoint(jsonObject, b);
		singleResult[2] = this.classifyWekaGlobal(jsonObject, b);
		singleResult[3] = this.classifyWekaRaster(jsonObject, b);
		singleResult[4] = this.classifyWekaCombined(jsonObject, b);

		for (int i = 0; i < singleResult.length; i++) {
			if (singleResult[i] != null) {
				for (int j = 0; j < singleResult[i].length(); j++) {
					JSONObject objectInArray = singleResult[i].getJSONObject(j);
					if (this.COMBINE_OPTION == 1) {
						allCombined.add(objectInArray);
					} else {
						JSONObject newElement = new JSONObject();
						boolean foundFlag = false;
						for (Iterator<JSONObject> iterator = allCombined.iterator(); iterator.hasNext();) {
							JSONObject element = iterator.next();

							if (element.getInt("id") == objectInArray.getInt("id")) {
								newElement.put("id", element.getInt("id"));
								newElement.put("prob", (element.getDouble("prob")
										+ objectInArray.getDouble("prob") * weightVector[i]));
								if (element.has("combineSum")) {
									double combineSum = weightVector[i] + element.getDouble("combineSum");
									newElement.put("combineSum", combineSum);
								}
								
								if (element.has("rotation")) {
									int rotation = element.getInt("rotation");
									newElement.put("rotation", rotation);
								}

								foundFlag = true;

								iterator.remove();
							}
						}
						if (foundFlag) {
							allCombined.add(newElement);

						} else {
							double prob = objectInArray.getDouble("prob");
							objectInArray.remove("prob");
							objectInArray.put("combineSum", weightVector[i]);

							objectInArray.put("prob", prob * weightVector[i]);
							allCombined.add(objectInArray);
						}

					}
				}
			}
		}

		Collections.sort(allCombined, this.probCompare);
		JSONArray sortedResultJson = new JSONArray();
		ArrayList<Integer> blackList = new ArrayList<Integer>();
		int count = 0;
		int found = 0;

		double finalProb = 1;
		JSONObject firstObj = allCombined.get(0);
		if (this.COMBINE_OPTION == 0 && firstObj.has("combineSum")) {

			double sumCombined = firstObj.getDouble("combineSum");
			if (sumCombined != 0) {
				finalProb = sumCombined; // / (double) sumCombined;
			} else {
				finalProb = 1;
			}

		}

		while (found < Math.min(10, allCombined.size())) {
			JSONObject obj = allCombined.get(count);
			int id = (int) obj.get(SQLIdentifier.IMAGES_ID);
			boolean inBlackList = false;
			for (Integer i : blackList) {
				if (id == i) {
					inBlackList = true;
				}
			}
			if (!inBlackList) {
				double sumProb = obj.getDouble("prob");
				obj.remove("prob");
				obj.remove("combineSum");
				obj.put("prob", sumProb / finalProb);

				sortedResultJson.put(obj);
				blackList.add(id);
				found++;
			}

			count++;

		}

		String hamming = jsonObject.getString("hamming");
		double rotation =  jsonObject.getDouble("rotation");

		JSONObject obj = new JSONObject();
		obj.put("rotation",rotation);
		
		Point2D gravity = Preprocessor.getGravityCenter(hamming);
		obj.put("gravityX", gravity.getX());
		obj.put("gravityY", gravity.getY());
		resultJson = SQLtoJSON.getResultsfromID(sortedResultJson, 10);
		resultJson.put(obj);

		return resultJson;
	}

	private JSONArray classifyRaster(JSONObject jsonObject, boolean f) {
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup("java:comp/env/jdbc/myds");

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		JSONArray resultJson = null;
		JSONArray rankedJson = null;

		Ranker hammingranker = new HammingRanker();
		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			String EXECUTE_QUERY_HAMMING = "select " + SQLIdentifier.HAMMING_ID + "," + SQLIdentifier.HAMMING_CONTENT
					+ " from " + SQLIdentifier.DATABASE_NAME_HAMMING;
			rs = stmt.executeQuery(EXECUTE_QUERY_HAMMING);
			rankedJson = ((HammingRanker) hammingranker).rank(rs, jsonObject.getString("hamming"));

			if (rankedJson != null) {
				if (f) {
					return rankedJson;
				}
				resultJson = SQLtoJSON.getResultsfromID(rankedJson, 10);

			}

			JSONObject obj = new JSONObject();
			obj.put("symbolID", 0);
			resultJson.put(obj);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				/* ignored */ }
			try {
				stmt.close();
			} catch (Exception e) {
				/* ignored */ }
			try {
				conn.close();
			} catch (Exception e) {
				/* ignored */ }
		}

		return resultJson;

	}

	private JSONArray classifyPoint(JSONObject jsonObject, boolean f) {
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup("java:comp/env/jdbc/myds");

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		JSONArray resultJson = null;

		Ranker pointRanker = new PointRanker();
		String inputFeaturePoints = null;
		ArrayList<Point2D> featurePoints = null;
		try {

			conn = myDS.getConnection();
			stmt = conn.createStatement();

			int windowSize = 500; // todo: get from HTTP

			int invariance = jsonObject.getInt("rotationInvariance");

			boolean invarianceFlag = true;
			if (invariance == 0) {
				invarianceFlag = false;
			}
			inputFeaturePoints = jsonObject.getString("featurePoints");
			ImageScaleInstance isi = Preprocessor.getScaledCurvaturePointsFromString(inputFeaturePoints, windowSize);
			featurePoints = isi.getFeaturePoints();

			String EXECUTE_QUERY_SVG = "select " + SQLIdentifier.SVG_ID + "," + SQLIdentifier.SVG_NORMALIZEDPOINTS
					+ " from " + SQLIdentifier.DATABASE_NAME_SVG;
			rs = stmt.executeQuery(EXECUTE_QUERY_SVG);

			JSONArray rankedPointArray = ((PointRanker) pointRanker).rank(rs, featurePoints, invarianceFlag);

			double rotation =  Preprocessor.getRotation(jsonObject.getString("hamming"));
			System.out.println("rotation: " + rotation);
			
			if (rankedPointArray != null) {
				resultJson = SQLtoJSON.getResultsfromID(rankedPointArray, 10);

				if (f) {
					return resultJson;
				}

				JSONObject obj = new JSONObject();
				obj.put("minX", isi.getMinX());
				obj.put("minY", isi.getMinY());
				obj.put("scaleFactor", isi.getScaleFactor());
				obj.put("xOffset", isi.getxOffset());
				obj.put("yOffset", isi.getyOffset());

				obj.put("rotation",rotation);
				resultJson.put(obj);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				/* ignored */ }
			try {
				stmt.close();
			} catch (Exception e) {
				/* ignored */ }
			try {
				conn.close();
			} catch (Exception e) {
				/* ignored */ }
		}
		return resultJson;
	}

	private  JSONArray classifyWekaGlobal(JSONObject jsonObject, boolean f) {
		JSONArray resultJson = null;

		JSONObject obj = getInformationObject(jsonObject);

		ServletContext context = this.getServletContext();
		this.dataSet = (Instances) context.getAttribute("dataSet");
		this.classifier = (Classifier) context.getAttribute("classifier");

		Instance instance = new DenseInstance(9);
		instance.setValue((Attribute) this.dataSet.attribute(1), obj.getInt("rasterPointSum"));
		instance.setValue((Attribute) this.dataSet.attribute(2), obj.getDouble("ratio"));
		instance.setValue((Attribute) this.dataSet.attribute(3), obj.getInt("curvaturePointSum"));
		instance.setValue((Attribute) this.dataSet.attribute(4), obj.getDouble("degreeSum"));
		instance.setValue((Attribute) this.dataSet.attribute(5), obj.getDouble("averageDistance"));
		instance.setValue((Attribute) this.dataSet.attribute(6), obj.getDouble("length"));
		instance.setValue((Attribute) this.dataSet.attribute(7), obj.getDouble("rasterProp"));
		instance.setValue((Attribute) this.dataSet.attribute(8), obj.getInt("segments"));
		// add the instance
		this.dataSet.add(instance);
		instance.setDataset(this.dataSet);
		JSONArray rankedArray = null;
		try {
			double[] fDistribution = this.classifier.distributionForInstance(instance);
			Ranker easyRanker = new EasyRanker();
			rankedArray = ((EasyRanker) easyRanker).rank(fDistribution, this.classifier, this.dataSet);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (rankedArray != null) {
			if (f) {
				return rankedArray;
			}
			resultJson = SQLtoJSON.getResultsfromID(rankedArray, 10);
		}
		resultJson.put(obj);

		return resultJson;

	}

	private JSONArray classifyWekaRaster(JSONObject jsonObject, boolean f) {
		JSONArray resultJson = null;

		JSONObject obj = getInformationObject(jsonObject);

		ServletContext context = this.getServletContext();
		this.rasterDataSet = (Instances) context.getAttribute("rasterDataSet");
		this.rasterClassifier = (Classifier) context.getAttribute("rasterClassifier");
		Instance instance = new DenseInstance(627);
		instance.setValue((Attribute) this.rasterDataSet.attribute(1), obj.getInt("rasterPointSum"));
		String s = obj.getString("raster625");
		char[] bits = s.toCharArray();
		for (int i = 0; i < bits.length; i++) {
			char b = bits[i];
			if (b == '0') {
				instance.setValue((Attribute) this.rasterDataSet.attribute(i + 2), 0);
			} else {
				instance.setValue((Attribute) this.rasterDataSet.attribute(i + 2), 1);
			}

		}
		this.rasterDataSet.add(instance);
		instance.setDataset(this.rasterDataSet);

		JSONArray rankedArray = null;
		try {
			double[] fDistribution = this.rasterClassifier.distributionForInstance(instance);
			Ranker easyRanker = new EasyRanker();
			rankedArray = ((EasyRanker) easyRanker).rank(fDistribution, this.rasterClassifier, this.rasterDataSet);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (rankedArray != null) {
			if (f) {
				return rankedArray;
			}
			resultJson = SQLtoJSON.getResultsfromID(rankedArray, 10);
		}
		resultJson.put(obj);
		return resultJson;

	}

	private JSONArray classifyWekaCombined(JSONObject jsonObject, boolean f) {
		JSONArray resultJson = null;
		JSONObject obj = getInformationObject(jsonObject);

		ServletContext context = this.getServletContext();
		this.localDataset = (Instances) context.getAttribute("localDataSet");
		System.out.println(this.localDataset.size());
		this.localClassifier = (Classifier) context.getAttribute("localClassifier");

		Instance instance = new DenseInstance(111);
		instance.setValue((Attribute) this.localDataset.attribute(1), obj.getInt("rasterPointSum"));
		instance.setValue((Attribute) this.localDataset.attribute(2), obj.getDouble("ratio"));
		instance.setValue((Attribute) this.localDataset.attribute(3), obj.getInt("curvaturePointSum"));
		instance.setValue((Attribute) this.localDataset.attribute(4), obj.getDouble("degreeSum"));
		instance.setValue((Attribute) this.localDataset.attribute(5), obj.getDouble("averageDistance"));
		instance.setValue((Attribute) this.localDataset.attribute(6), obj.getDouble("length"));
		instance.setValue((Attribute) this.localDataset.attribute(7), obj.getDouble("rasterProp"));
		instance.setValue((Attribute) this.localDataset.attribute(8), obj.getInt("segments"));
		instance.setValue((Attribute) this.localDataset.attribute(9), obj.getDouble("gravityX"));
		instance.setValue((Attribute) this.localDataset.attribute(10), obj.getDouble("gravityY"));

		String s = obj.getString("curveraster100");

		int curveRasterSize = 10;
		int curveRasterSizeSq = curveRasterSize * curveRasterSize;
		char[] bits = s.toCharArray();

		if (curveRasterSizeSq == 25) {
			String sShort = "";
			for (int y = 0; y < curveRasterSize; y++) {
				for (int x = 0; x < curveRasterSize; x++) {
					int a = Character.getNumericValue(bits[x * 2 + y * 2 * 10]);
					int b = Character.getNumericValue(bits[(x * 2 + 1) + (y * 2) * 10]);
					int c = Character.getNumericValue(bits[(x * 2) + (y * 2 + 1) * 10]);
					int d = Character.getNumericValue(bits[(x * 2 + 1) + (y * 2 + 1) * 10]);
					sShort += Math.min(5, a + b + c + d) + "";
				}
			}
			bits = sShort.toCharArray();
		}

		for (int i = 0; i < bits.length; i++) {
			char b = bits[i];
			int number = Character.getNumericValue(b);
			instance.setValue((Attribute) this.localDataset.attribute(i + 11), number);
		}

		this.localDataset.add(instance);
		instance.setDataset(this.localDataset);

		JSONArray rankedArray = null;
		try {
//			System.out.println(instance);
			double[] fDistribution = this.localClassifier.distributionForInstance(instance);
			Ranker easyRanker = new EasyRanker();
			rankedArray = ((EasyRanker) easyRanker).rank(fDistribution, this.localClassifier, this.localDataset);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (rankedArray != null) {
			if (f) {
				return rankedArray;
			}
			resultJson = SQLtoJSON.getResultsfromID(rankedArray, 10);
			resultJson.put(obj);

		}

		return resultJson;

	}

	private JSONArray classifyRanked(JSONObject jsonObject) {
		JSONArray resultJson = null;
		JSONObject obj = getInformationObject(jsonObject);
		int classificationIndex = jsonObject.getInt("classificator");
		ServletContext context = this.getServletContext();
		this.dataSet = (Instances) context.getAttribute("dataSet");
		this.classifier = (Classifier) context.getAttribute("classifier");

		Instance instance = new DenseInstance(9);
		instance.setValue((Attribute) this.dataSet.attribute(1), obj.getInt("rasterPointSum"));
		instance.setValue((Attribute) this.dataSet.attribute(2), obj.getDouble("ratio"));
		instance.setValue((Attribute) this.dataSet.attribute(3), obj.getInt("curvaturePointSum"));
		instance.setValue((Attribute) this.dataSet.attribute(4), obj.getDouble("degreeSum"));
		instance.setValue((Attribute) this.dataSet.attribute(5), obj.getDouble("averageDistance"));
		instance.setValue((Attribute) this.dataSet.attribute(6), obj.getDouble("length"));
		instance.setValue((Attribute) this.dataSet.attribute(7), obj.getDouble("rasterProp"));
		instance.setValue((Attribute) this.dataSet.attribute(8), obj.getInt("segments"));
		// add the instance

		JSONArray rankedArray = null;
		Instances notFilteredInstances = this.dataSet;
		Classifier filteredClassifier = this.classifier;

		try {
			int[][] results = new int[10][2];
			for (int i = 0; i < 10; i++) {
				notFilteredInstances.add(instance);
				instance.setDataset(notFilteredInstances);
				double[] fDistribution = filteredClassifier.distributionForInstance(instance);
				// System.out.println("possible classes = " +
				// fDistribution.length);
				int maxIndex = 0;
				for (int j = 1; j < fDistribution.length; j++) {
					// System.out.println(
					// "index " + j + ":" +
					// Integer.valueOf(notFilteredInstances.classAttribute().value(j)));
					if (fDistribution[j] > fDistribution[maxIndex]) {
						maxIndex = j;
					}
				}
				results[i][0] = Integer.valueOf(notFilteredInstances.classAttribute().value(maxIndex));
				results[i][1] = (int) (fDistribution[maxIndex] * 100);
				// System.out.println("Cut index: " + results[i][0] + ":
				// " + results[i][1]);

				RemoveWithValues filter = new RemoveWithValues();
				String[] options = new String[5];
				options[0] = "-C";
				options[1] = "1";
				options[2] = "-L";
				options[3] = String.valueOf(maxIndex + 1);
				options[4] = "-H";

				filter.setOptions(options);
				filter.setInputFormat(notFilteredInstances);
				Instances filteredInstances = Filter.useFilter(notFilteredInstances, filter);
				notFilteredInstances = filteredInstances;
				Classifier newClassifier = Classificators.generateClassifier(notFilteredInstances, classificationIndex);
				newClassifier.buildClassifier(notFilteredInstances);
				filteredClassifier = newClassifier;

			}

			Ranker easyRanker = new EasyRanker();
			rankedArray = ((EasyRanker) easyRanker).rankArray(results);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (rankedArray != null) {
			resultJson = SQLtoJSON.getResultsfromID(rankedArray, 10);
		}
		resultJson.put(obj);
		return resultJson;
	}
	
	private JSONArray classifyGeoViewerRequest(JSONObject jsonObject) {
		int size = jsonObject.getInt("size");
		JSONArray resultJson = null;
		
		String mls = (String) jsonObject.get("data");
//		System.out.println("request: " + mls);
		List<LineString> lineStrings = JsonToLineString(mls, size);
		String rasterString = Rasterizer.rasterize(lineStrings, size);
		double rotation = Preprocessor.getRotation(rasterString);

		List<LineString> rotatedLineStrings = Preprocessor.rotateLineStrings(lineStrings, rotation);
		
		String rotatedRasterString = Rasterizer.rasterize(rotatedLineStrings, size);

		jsonObject.put("rotation", rotation);
		jsonObject.put("hamming", rotatedRasterString);
		jsonObject.put("featurePoints", Curvature.calculateCurvaturePoints(rotatedLineStrings));
		jsonObject.put("startendPoints", Curvature.calculateStartEndPoints(rotatedLineStrings));


//		printHammingString(hamming, size);
		
		int rotationInvariance = (int) jsonObject.get("rotationInvariance");
		boolean rotationInvarianceFlag = false;
		if (rotationInvariance == 1) {
			rotationInvarianceFlag = true;
		}
		
//		return this.classifyPoint(jsonObject, rotationInvarianceFlag);
		return this.classifyAllCombined(jsonObject, true);
	}
	
	private static void printHammingString(String hamming, int size) {
		System.out.println("Haming:");
		System.out.println("");
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.print(hamming.charAt(i*size + j) + " ");
			}
			System.out.println("");
		}
		System.out.println("----------------------------------------");

	}
	
	public static List<LineString> JsonToLineString(String mls, int size) {
		List<LineString> lineStrings = new ArrayList<LineString>();
//		System.out.println(mls);
		String[] lineStringStrings = mls.split("\\|");
		GeometryFactory geometryFactory = new GeometryFactory();

		for (int i = 0; i < lineStringStrings.length; i++) {
			String[] singleLineString = lineStringStrings[i].split("]");
			Coordinate[] coords = new Coordinate[singleLineString.length];
			for (int j = 0; j < coords.length; j++) {
				String[] coordinateString = singleLineString[j].split(",");
				coords[j] = new Coordinate(Double.parseDouble(coordinateString[0]), Double.parseDouble(coordinateString[1]));
			}
			lineStrings.add(geometryFactory.createLineString(coords));
		}

		return lineStrings;
	}

	private JSONArray classifyGeo(JSONObject jsonObject) {
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup("java:comp/env/jdbc/myds");

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		JSONArray resultJson = null;
		JSONArray rankedJson = null;

		Ranker geoRanker = new GeoRanker();
		String inputFeaturePoints = null;
		ArrayList<Point2D> featurePoints = null;

		int windowSize = 500; // todo: get from HTTP
		int invariance = jsonObject.getInt("rotationInvariance");
		int nearPointFilter = jsonObject.getInt("nearPointFilter");

		boolean invarianceFlag = true;
		if (invariance == 0) {
			invarianceFlag = false;
		}
		inputFeaturePoints = jsonObject.getString("featurePoints");

		ImageScaleInstance isi = Preprocessor.getScaledCurvaturePointsFromString(inputFeaturePoints, windowSize);
		featurePoints = isi.getFeaturePoints();

		try {
			conn = myDS.getConnection();
			stmt = conn.createStatement();
			String EXECUTE_QUERY_GEO = "select " + SQLIdentifier.GEO_ID + "," + SQLIdentifier.GEO_IDGEO + ","
					+ SQLIdentifier.GEO_NORMALIZEDPOINTS + " from " + SQLIdentifier.DATABASE_NAME_GEO + " where "
					+ SQLIdentifier.GEO_ID + " < 225";
			rs = stmt.executeQuery(EXECUTE_QUERY_GEO);
			rankedJson = ((GeoRanker) geoRanker).rank(rs, featurePoints, invarianceFlag, nearPointFilter);

			resultJson = SQLtoJSON.getGeoResultsfromID(rankedJson, 10);

			JSONObject obj = new JSONObject();
			obj.put("symbolID", 0);

			obj.put("minX", isi.getMinX());
			obj.put("minY", isi.getMinY());
			obj.put("scaleFactor", isi.getScaleFactor());
			obj.put("xOffset", isi.getxOffset());
			obj.put("yOffset", isi.getyOffset());

			resultJson.put(obj);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				/* ignored */ }
			try {
				stmt.close();
			} catch (Exception e) {
				/* ignored */ }
			try {
				conn.close();
			} catch (Exception e) {
				/* ignored */ }
		}
		return resultJson;
	}

	public static JSONObject getInformationObject(JSONObject jsonObject) {
		String startendPoints = null;
		String featurePoints = null;
		int windowSize = 500; // todo: get from HTTP

		startendPoints = jsonObject.getString("startendPoints");
		featurePoints = jsonObject.getString("featurePoints");

		ImageScaleInstance isi = Preprocessor.getScaledCurvaturePointsFromString(featurePoints, windowSize);

		Point2D gravity = Preprocessor.getGravityCenter(jsonObject.getString("hamming"));

		if (gravity.getX() < 0 || gravity.getX() > 500 || Double.isNaN(gravity.getX())
				|| Double.isInfinite(gravity.getX())) {
			gravity.setLocation(250, 250);
		}
		if (gravity.getY() < 0 || gravity.getY() > 500 || Double.isNaN(gravity.getY())
				|| Double.isInfinite(gravity.getY())) {
			gravity.setLocation(250, 250);
		}

		// System.out.println("Length: " + jsonObject.get + " |
		// scaleFactor: " + isi.getScaleFactor());

		JSONObject obj = new JSONObject();
		obj.put("classID", -1);
		obj.put("raster625", jsonObject.getString("hamming"));
		obj.put("rasterPointSum", Preprocessor.countHammingString(jsonObject.getString("hamming")));
		obj.put("ratio", isi.getRatio());
		obj.put("curvaturePointSum", isi.getFeaturePoints().size());
		obj.put("degreeSum", Preprocessor.degreeSumBetweenPoints(isi.getFeaturePoints()));
		obj.put("averageDistance", Preprocessor.calculateAveragePointDistance(isi.getFeaturePoints()));
		obj.put("image", jsonObject.get("data"));
		obj.put("length", isi.getLength());
		obj.put("rasterProp", Preprocessor.calcRasterProp(jsonObject.getString("hamming")));
		obj.put("segments", Preprocessor.getSegmentAmount(startendPoints));
		obj.put("gravityX", gravity.getX());
		obj.put("gravityY", gravity.getY());
		obj.put("curveraster100", Preprocessor.getCurveRaster(isi.getFeaturePoints(), windowSize));

		return obj;
	}


}