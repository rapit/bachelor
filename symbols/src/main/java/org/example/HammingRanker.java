package org.example;

import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

import org.json.JSONArray;
import org.json.JSONObject;

import org.example.util.*;

public class HammingRanker extends Ranker {

	public HammingRanker(Strategy s, String idName, String contentName, String probName) {
		super(s, idName, contentName, probName);
	}

	public HammingRanker() {
		super(Strategy.GREEDY, SQLIdentifier.HAMMING_CONTENT, SQLIdentifier.HAMMING_ID, "prob");
	}

	private double getHamming(byte[] bytes, String hamming) {
		int length = hamming.length();

		BitSet bits = new BitSet(bytes.length);
		for (int i = 0; i < bytes.length; i++) {
			if ((bytes[i]) != 48) {
				bits.set(i);
			}
		}
		BitSet hammingbits = new BitSet(length);
		for (int i = 0; i < length; i++) {
			if (hamming.charAt(i) == '1') {
				hammingbits.set(i);

			}
		}
		int maxsum = hammingbits.cardinality();

		// System.out.println("hamming: " + hammingbits);
		// System.out.println("bytes: " + bits);
		// System.out.println("");
		hammingbits.and(bits);

		int sum = hammingbits.cardinality();
		double result = Math.round((double) sum / maxsum * 100);
		// System.out.println("sum: " + sum + " | maxsum: " + maxsum + "|
		// result: " + result);

		return result;
	}

	private double getXYDistance(byte[] bytes, String hamming) {
		int length = hamming.length();
		int sqlength = (int) Math.sqrt(length);
		String byteString = "";
		String hammingString = "";

//		System.out.println(sqlength);

		for (int i = 0; i < hamming.length(); i++) {
			if ((i > 0) && (i % sqlength) == 0) {
				hammingString += "s";
			}
			hammingString += hamming.charAt(i);

		}
//		System.out.println(hammingString);

		for (int i = 0; i < bytes.length; i++) {
			if ((i > 0) && (i % sqlength) == 0) {
				byteString += "s";
			}
			if ((bytes[i]) != 48) {
				byteString += "1";
			} else {
				byteString += "0";

			}
		}
		double compareA = 0.0;
		double compareB = 0.0;
		try {
			BitMatrix aMatrix = BitMatrix.parse(byteString, "1", "0");
			BitMatrix bMatrix = BitMatrix.parse(hammingString, "1", "0");
//			System.out.println(bMatrix);
			compareA = org.example.util.BitMatrix.distance(aMatrix, bMatrix);
			compareB = org.example.util.BitMatrix.distance(bMatrix, aMatrix);
		}catch (IllegalArgumentException e) {
			
		}

//		System.out.println(
//				aMatrix.getWidth() + "|" + aMatrix.getHeight() + "||" + bMatrix.getWidth() + "|" + bMatrix.getHeight());



//		System.out.println(compareA);
//		System.out.println(compareB);
		return calculateP((compareA + compareB) / 2);

	}

	private static double calculateP(double x) { //e function for result calculation
		double factor = 0.5;
		double curvature = 0.25;
		if (x > 1.122) {
			factor = 1.2;
			curvature = 1;
		}
		return (factor * Math.exp((-curvature * x) + Math.log(200)));

	}

	public JSONArray rank(ResultSet rs, String hamming) throws SQLException {
		// System.out.println(hamming);
		JSONArray resultJson = new JSONArray();

		List<JSONObject> jsonValues = new ArrayList<JSONObject>();

		JSONArray sortedResultJson = new JSONArray();

		ResultSetMetaData rsmd = rs.getMetaData();
		String typeID = rsmd.getColumnTypeName(1);
		String typeContent = rsmd.getColumnTypeName(2);

		while (rs.next()) {
			JSONObject obj = new JSONObject();
			if (typeID == "INT") {
				obj.put(SQLIdentifier.HAMMING_ID, rs.getInt(1));
			}
			if (typeContent == JDBCType.BLOB.getName()) {
				double XYdistance = getXYDistance(rs.getBytes(2), hamming);
				if (Double.isFinite(XYdistance)) {
					obj.put(this.cNameProb, XYdistance);

				} else {
					obj.put(this.cNameProb, 0.0);
				}

			}
			jsonValues.add(obj);

		}

		Collections.sort(jsonValues, this.probCompare);
		for (int i = 0; i < 10; i++) { // jsonValues.size()
			sortedResultJson.put(jsonValues.get(i));
		}

		return sortedResultJson;
	}

}
