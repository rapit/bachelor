package org.example;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.bind.DatatypeConverter;

import org.eclipse.jetty.webapp.WebAppContext;
import org.example.util.Curvature;
import org.example.util.Rasterizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.LineString;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.OneR;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.LMT;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.RandomTree;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.experiment.InstanceQuery;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;

public class TrainingServlet extends HttpServlet {

	private static final String DATABASE = "java:comp/env/jdbc/myds";

	private Instances dataSet;
	private Instances rasterDataSet;
	private Instances localDataset;

	private Classifier classifier;
	private Classifier rasterClassifier;
	private Classifier localClassifier;

	private static final int NUM_INSTANCES = 700;
	private static final int CURVE_RASTER_SIZE = 10;

	public TrainingServlet() {
		try {
			super.init();

		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void init() throws ServletException {
		updateDataSet(-1);
//		System.out.println("skipping initialisation");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	//Classification request
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("success", "yes");
		String param = request.getParameter("arff");
		String param2 = request.getParameter("classificationMode");
		try {
			int mode = Integer.valueOf(param);
			int classificationMode = Integer.valueOf(param2);
			switch (mode) {
			case 0: {
				updateDataSet(classificationMode);
				Instances newDataSet = this.dataSet;

				if (newDataSet != null) {

					File f = new File("output.arff");
					ArffSaver saver = new ArffSaver();
					saver.setInstances(newDataSet);
					saver.setFile(f);
					saver.writeBatch();
					FileInputStream is = null;
					OutputStream os = null;
					try {
						response.setContentLength((int) f.length());

						is = new FileInputStream(f);
						os = response.getOutputStream();
						int bytes;
						while ((bytes = is.read()) != -1) {
							os.write(bytes);
						}
					} catch (IOException e) {
						// throw new ApplicationException("IOException in
						// populateWithJSON",
						// e);
					} finally {
						is.close();
						os.close();
					}
				}
			}
			case 1: {
				updateDataSet(classificationMode);
				Instances newDataSet = this.rasterDataSet;

				if (newDataSet != null) {

					File f = new File("output.arff");
					ArffSaver saver = new ArffSaver();
					saver.setInstances(newDataSet);
					saver.setFile(f);
					saver.writeBatch();
					FileInputStream is = null;
					OutputStream os = null;
					try {
						response.setContentLength((int) f.length());

						is = new FileInputStream(f);
//						
//						StringBuilder builder = new StringBuilder();
//						int ch;
//						while((ch = is.read()) != -1){
//						    builder.append((char)ch);
//						}

//						System.out.println(builder.toString());
						
						
						os = response.getOutputStream();
						int bytes;
						while ((bytes = is.read()) != -1) {
							os.write(bytes);
						}
					} catch (IOException e) {
						// throw new ApplicationException("IOException in
						// populateWithJSON",
						// e);
					} finally {
						is.close();
						os.close();
					}
				}
				break;
			}
			case 2: {
				updateDataSet(classificationMode);
				break;
			}
			case 3: {
				updateDataSet(classificationMode);
				Instances newDataSet = this.localDataset;

				if (newDataSet != null) {

					File f = new File("output.arff");
					ArffSaver saver = new ArffSaver();
					saver.setInstances(newDataSet);
					saver.setFile(f);
					saver.writeBatch();
					FileInputStream is = null;
					OutputStream os = null;
					try {
						response.setContentLength((int) f.length());

						is = new FileInputStream(f);
						os = response.getOutputStream();
						int bytes;
						while ((bytes = is.read()) != -1) {
							os.write(bytes);
						}
					} catch (IOException e) {
						// throw new ApplicationException("IOException in
						// populateWithJSON",
						// e);
					} finally {
						is.close();
						os.close();
					}
				}
				break;
			}

			default: {

			}
			}

		} catch (NumberFormatException e) {

		}

	}

	private Instances generateRasterDataSet() {
		InstanceQuery query;
		try {
			query = new InstanceQuery();
			query.setQuery("select " + SQLIdentifier.TRAINING_CLASS_ID + "," + SQLIdentifier.TRAINING_WEIGHT + ","
					+ SQLIdentifier.TRAINING_RASTER625 + "," + SQLIdentifier.TRAINING_RASTER_POINTSUM + " from "
					+ SQLIdentifier.DATABASE_NAME_TRAINING);
			Instances dataSet = query.retrieveInstances();

			int wIndex = 1;
			int weightSum = 0;
			for (int i = 0; i < dataSet.numInstances(); i++) {
				double weight = dataSet.instance(i).value(wIndex);
				weightSum += weight;
				dataSet.instance(i).setWeight(weight);
			}
			int num = dataSet.numAttributes();
			List<String> values = new ArrayList<String>(2);
			values.add("0");
			values.add("1");
			for (int i = 0; i < 625; i++) {
				dataSet.insertAttributeAt(new Attribute("r" + (i + 1), values), num + i);
			}

			int hIndex = 2;
			for (int i = 0; i < dataSet.numInstances(); i++) {
				String s = dataSet.instance(i).stringValue(hIndex);
				char[] bits = s.toCharArray();
				for (int j = 0; j < Math.min(625, bits.length); j++) {
					char b = bits[j];
					if (b == '0') {
						dataSet.instance(i).setValue(4 + j, 0);
					} else {
						dataSet.instance(i).setValue(4 + j, 1);

					}
				}

			}

			dataSet.deleteAttributeAt(hIndex);
			dataSet.deleteAttributeAt(wIndex);

			NumericToNominal convert = new NumericToNominal();
			String[] options = new String[2];
			options[0] = "-R";
			options[1] = "1"; // range of variables to make numeric

			convert.setOptions(options);
			convert.setInputFormat(dataSet);

			Instances newDataSet = Filter.useFilter(dataSet, convert);
			newDataSet.setClassIndex(0);

			return newDataSet;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private Instances generateLocalDataSet() {
		InstanceQuery query;
		try {
			query = new InstanceQuery();
			query.setQuery("select " + SQLIdentifier.TRAINING_CLASS_ID + "," + SQLIdentifier.TRAINING_WEIGHT + ","
					+ SQLIdentifier.TRAINING_RASTER_POINTSUM + "," + SQLIdentifier.TRAINING_RATIO + ","
					+ SQLIdentifier.TRAINING_CURVATURE_POINTSUM + "," + SQLIdentifier.TRAINING_DEGREE_SUM + ","
					+ SQLIdentifier.TRAINING_AVERGAE_DISTANCE + "," + SQLIdentifier.TRAINING_LENGTH + ","
					+ SQLIdentifier.TRAINING_RASTER_PROP + "," + SQLIdentifier.TRAINING_SEGMENTS + ","
					+ SQLIdentifier.TRAINING_GRAVITY_X + "," + SQLIdentifier.TRAINING_GRAVITY_Y + ","
					+ SQLIdentifier.TRAINING_CURVERASTER + " from " + SQLIdentifier.DATABASE_NAME_TRAINING);
			Instances dataSet = query.retrieveInstances();

			int wIndex = 1;
			int weightSum = 0;
			for (int i = 0; i < dataSet.numInstances(); i++) {
				double weight = dataSet.instance(i).value(wIndex);
				weightSum += weight;
				dataSet.instance(i).setWeight(weight);
			}
			dataSet.deleteAttributeAt(wIndex);

			NumericToNominal convert = new NumericToNominal();
			String[] options = new String[2];
			options[0] = "-R";
			options[1] = "1"; // range of variables to make numeric

			convert.setOptions(options);
			convert.setInputFormat(dataSet);

			Instances newDataSet = Filter.useFilter(dataSet, convert);

			int num = newDataSet.numAttributes();

			List<String> values = new ArrayList<String>(2);
			values.add("0");
			values.add("1");
			values.add("2");
			values.add("3");
			values.add("4");
			values.add("5");
			
			int size = TrainingServlet.CURVE_RASTER_SIZE;
			int sqSize = TrainingServlet.CURVE_RASTER_SIZE * TrainingServlet.CURVE_RASTER_SIZE;
			for (int i = 0; i < sqSize; i++) {
				newDataSet.insertAttributeAt(new Attribute("r" + (i + 1), values), num + i);
			}

			int hIndex = 11;
			int numInstances = newDataSet.numInstances();
			int deleteCounter = 0;
			for (int i = 0; i < numInstances; i++) {
				String s = newDataSet.instance(i - deleteCounter).stringValue(hIndex);
//				String s2 = newDataSet.instance(i - deleteCounter).stringValue(0);
				if (s.length() != 100) { // || s2.equals("1007") || s2.equals("1016") || s2.equals("1017")) {
					newDataSet.delete(i - deleteCounter);
					deleteCounter++;
				} else {
					char[] bits = s.toCharArray();
					
					if (sqSize == 25) {
						String sShort = "";
						for (int y = 0; y < size; y++) {
							for (int x = 0; x < size; x++) {
							int a = Character.getNumericValue(bits[x*2 + y*2 * 10]);
							int b = Character.getNumericValue(bits[(x*2+1) + (y*2) * 10]);
							int c = Character.getNumericValue(bits[(x*2) + (y*2+1) * 10]);
							int d = Character.getNumericValue(bits[(x*2+1) + (y*2+1) * 10]);
							sShort += Math.min(5, a+b+c+d) + "";
							}
						}
						bits = sShort.toCharArray();						
					}
					
					for (int j = 0; j < Math.min(sqSize, bits.length); j++) {
						char b = bits[j];
						newDataSet.instance(i - deleteCounter).setValue(12 + j,
								Math.min(5, Character.getNumericValue(b)));

					}
				}
			}

			newDataSet.deleteAttributeAt(hIndex);

			newDataSet.setClassIndex(0);
			System.out.println("local feature database size: " + newDataSet.size());
			return newDataSet;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private Instances generateDataSet() {
		InstanceQuery query;
		try {
			query = new InstanceQuery();
			query.setQuery("select " + SQLIdentifier.TRAINING_CLASS_ID + "," + SQLIdentifier.TRAINING_WEIGHT + ","
					+ SQLIdentifier.TRAINING_RASTER_POINTSUM + "," + SQLIdentifier.TRAINING_RATIO + ","
					+ SQLIdentifier.TRAINING_CURVATURE_POINTSUM + "," + SQLIdentifier.TRAINING_DEGREE_SUM + ","
					+ SQLIdentifier.TRAINING_AVERGAE_DISTANCE + "," + SQLIdentifier.TRAINING_LENGTH + ","
					+ SQLIdentifier.TRAINING_RASTER_PROP + "," + SQLIdentifier.TRAINING_SEGMENTS + " from "
					+ SQLIdentifier.DATABASE_NAME_TRAINING + " where " + SQLIdentifier.TRAINING_UNIQUE_ID + " < "
					+ TrainingServlet.NUM_INSTANCES);
			Instances dataSet = query.retrieveInstances();

			int wIndex = 1;
			int weightSum = 0;
			for (int i = 0; i < dataSet.numInstances(); i++) {
				double weight = dataSet.instance(i).value(wIndex);
				weightSum += weight;
				dataSet.instance(i).setWeight(weight);
			}
			dataSet.deleteAttributeAt(wIndex);
//			System.out.println(weightSum);

			NumericToNominal convert = new NumericToNominal();
			String[] options = new String[2];
			options[0] = "-R";
			options[1] = "1"; // range of variables to make numeric

			convert.setOptions(options);
			convert.setInputFormat(dataSet);

			Instances newDataSet = Filter.useFilter(dataSet, convert);
			newDataSet.setClassIndex(0);

			return newDataSet;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private void updateDataSet(int mode) {
		this.dataSet = generateDataSet();
		this.classifier = Classificators.generateClassifier(this.dataSet, mode);

		this.rasterDataSet = generateRasterDataSet();
		this.rasterClassifier = Classificators.generateClassifier(this.rasterDataSet, mode);

		this.localDataset = generateLocalDataSet();
		this.localClassifier = Classificators.generateClassifier(this.localDataset, mode);
		// System.out.println(this.rasterDataSet.toSummaryString());

		ServletContext context = this.getServletContext();
		context.setAttribute("dataSet", this.dataSet);
		context.setAttribute("classifier", this.classifier);
		context.setAttribute("rasterDataSet", this.rasterDataSet);
		context.setAttribute("rasterClassifier", this.rasterClassifier);
		context.setAttribute("localDataSet", this.localDataset);
		context.setAttribute("localClassifier", this.localClassifier);

		System.out.println("updated Classifier - trainingset size: " + this.dataSet.size());

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(
				"   doPost called with URI: " + request.getRequestURI() + " and length: " + request.getContentLength());
		StringBuffer jb = new StringBuffer();
		String line = null;

		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);

			}
		} catch (IOException | IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jb.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println(jsonObject);

		response.setHeader("Content-Type", "text/plain");
		response.setHeader("success", "yes");
		TrainingServlet.addTrainingInstance(jsonObject);
	}

	private static void addTrainingInstance(JSONObject jsonObject) {
		InitialContext ic;
		DataSource myDS = null;
		try {
			ic = new InitialContext();
			myDS = (DataSource) ic.lookup(TrainingServlet.DATABASE);

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection conn = null;
		Statement stmt = null;

		try {
			conn = myDS.getConnection();

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String INSERT_ELEMENT = "insert into " + SQLIdentifier.DATABASE_NAME_TRAINING + " ("
				+ SQLIdentifier.TRAINING_CLASS_ID + "," + SQLIdentifier.TRAINING_RASTER625 + ","
				+ SQLIdentifier.TRAINING_RASTER_POINTSUM + "," + SQLIdentifier.TRAINING_RATIO + ","
				+ SQLIdentifier.TRAINING_CURVATURE_POINTSUM + "," + SQLIdentifier.TRAINING_DEGREE_SUM + ","
				+ SQLIdentifier.TRAINING_AVERGAE_DISTANCE + "," + SQLIdentifier.TRAINING_IMAGE + ","
				+ SQLIdentifier.TRAINING_LENGTH + "," + SQLIdentifier.TRAINING_RASTER_PROP + ","
				+ SQLIdentifier.TRAINING_SEGMENTS + "," + SQLIdentifier.TRAINING_WEIGHT + ","
				+ SQLIdentifier.TRAINING_GRAVITY_X + "," + SQLIdentifier.TRAINING_GRAVITY_Y + ","
				+ SQLIdentifier.TRAINING_CURVERASTER + ") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		java.sql.PreparedStatement ps = null;
		if (!jsonObject.has("image")) {
			jsonObject = collectAttributes(jsonObject);
		}
		
		
		String hammingRaster = jsonObject.getString("raster625");
		InputStream stream = new ByteArrayInputStream(hammingRaster.getBytes(StandardCharsets.UTF_8));

		String curveRaster = jsonObject.getString("curveraster100");
		InputStream curveStream = new ByteArrayInputStream(curveRaster.getBytes(StandardCharsets.UTF_8));

		String image = jsonObject.getString("image");
		InputStream imageStream = new ByteArrayInputStream(image.getBytes(StandardCharsets.UTF_8));

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		
		InputStream is = null;
	
		try {
//			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagedata));

//			ImageIO.write(bufferedImage, "png", os);
			
			is = new ByteArrayInputStream(os.toByteArray());


			conn.setAutoCommit(false);

			ps = conn.prepareStatement(INSERT_ELEMENT);
			ps.setInt(1, jsonObject.getInt("classID"));
			ps.setBinaryStream(2, stream, (int) hammingRaster.getBytes().length);
			ps.setInt(3, jsonObject.getInt("rasterPointSum"));
			ps.setDouble(4, jsonObject.getDouble("ratio"));
			ps.setInt(5, jsonObject.getInt("curvaturePointSum"));
			ps.setDouble(6, jsonObject.getDouble("degreeSum"));
			ps.setDouble(7, jsonObject.getDouble("averageDistance"));
			ps.setBinaryStream(8, imageStream, (int) image.getBytes().length);
			ps.setDouble(9, jsonObject.getDouble("length"));
			ps.setDouble(10, jsonObject.getDouble("rasterProp"));
			ps.setInt(11, jsonObject.getInt("segments"));
			ps.setInt(12, jsonObject.getInt("weight"));
			ps.setDouble(13, jsonObject.getDouble("gravityX"));
			ps.setDouble(14, jsonObject.getDouble("gravityY"));
			ps.setBinaryStream(15, curveStream, (int) curveRaster.getBytes().length);

			ps.executeUpdate();
			conn.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}

		}
	}

	private static JSONObject collectAttributes(JSONObject jsonObject) {		
		int classID = (int) jsonObject.get("method"); //method is classID in case of classification
		

		
		int size = jsonObject.getInt("size");
		
		String mls = (String) jsonObject.get("data");
		List<LineString> lineStrings = ClassificationServlet.JsonToLineString(mls, size);
		String rasterString = Rasterizer.rasterize(lineStrings, size);
		double rotation = Preprocessor.getRotation(rasterString);
		List<LineString> rotatedLineStrings = Preprocessor.rotateLineStrings(lineStrings, rotation);
		String rotetedRasterString = Rasterizer.rasterize(rotatedLineStrings, size);



		jsonObject.put("hamming", rotetedRasterString);
		jsonObject.put("featurePoints", Curvature.calculateCurvaturePoints(rotatedLineStrings));
		jsonObject.put("startendPoints", Curvature.calculateStartEndPoints(rotatedLineStrings));
		
		JSONObject informationObject = ClassificationServlet.getInformationObject(jsonObject);
		informationObject.put("weight", 1);
		informationObject.put("classID", classID);
			
		return informationObject;
	}
}
