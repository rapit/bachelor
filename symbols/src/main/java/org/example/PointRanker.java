package org.example;

import java.awt.geom.Point2D;
import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.SynchronousQueue;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.json.JSONArray;
import org.json.JSONObject;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.util.AffineTransformation;

public class PointRanker extends Ranker {

	protected int minDistanceBetweenPoints = 20;
	protected double weightN = 0.6; // sum to 1
	protected double weightK = 0.4;
	protected final int WINDOW_SIZE = 500;

	public JSONArray rank(ResultSet rs, ArrayList<Point2D> featurePoints, boolean invarianceFlag) throws SQLException {
		List<JSONObject> jsonValues = new ArrayList<JSONObject>();

		ArrayList<Point2D> databasePoints = null;

		JSONArray sortedResultJson = new JSONArray();

		ResultSetMetaData rsmd = rs.getMetaData();
		String typeID = rsmd.getColumnTypeName(1);
		String typePoints = rsmd.getColumnTypeName(2);

		int degStep = 20;

		if (!invarianceFlag) {
			degStep = 360;
		}

		while (rs.next()) {
			for (int rotDeg = 0; rotDeg < 360; rotDeg += degStep) {
				JSONObject obj = new JSONObject();
				if (typeID == "INT") {
					obj.put(SQLIdentifier.HAMMING_ID, rs.getInt(1));
				}
				if (typePoints == "VARCHAR") {
					String points = rs.getString(2);
					databasePoints = this.parsePoints(points);
					double probKNeighbours = 0.0;
					double probNNeighbours = 0.0;
					double prob = 0.0;
					
					ArrayList<Point2D> rotFeaturePoints = this.rotateFeaturePoints(featurePoints, rotDeg);
					String rotPointString = "";
					for (int j = 0; j < rotFeaturePoints.size(); j++) {
						rotPointString += rotFeaturePoints.get(j).getX() + "," + rotFeaturePoints.get(j).getY() + "|";
					}
					
					obj.put("rotatedPoints", rotPointString);
					
					if (databasePoints.size() > 0 && rotFeaturePoints.size() > 0 && invarianceFlag) {
						probKNeighbours = matchPoints(databasePoints, rotFeaturePoints);
						probNNeighbours = matchNearestPoints(databasePoints, rotFeaturePoints);	
					} else {
						probKNeighbours = matchPoints(databasePoints, featurePoints);
						probNNeighbours = matchNearestPoints(databasePoints, featurePoints);	
					}

					prob = this.weightK * probKNeighbours + this.weightN * probNNeighbours;
					// }
					obj.put(this.cNameProb, prob);
					// System.out.println("Prob: " + prob + "\n");

				}
				obj.put(this.cNameRotation, rotDeg);

				jsonValues.add(obj);
			}

		}

		Collections.sort(jsonValues, this.probCompare);
		
		ArrayList<Integer> blackList = new ArrayList<Integer>();
		int count = 0;
		int found = 0;
		while (found < Math.min(10, jsonValues.size())) {
			JSONObject obj = jsonValues.get(count);
			int id = (int) obj.get(SQLIdentifier.GEO_ID);
			boolean inBlackList = false;
			for (Integer i : blackList) {
				if (id == i) {
					inBlackList = true;
				}
			}
			if (!inBlackList) {
				sortedResultJson.put(obj);
				blackList.add(id);
				found++;
			}

			count++;

		}
//		for (int i = 0; i < 10; i++) { // jsonValues.size()
//			sortedResultJson.put(jsonValues.get(i));
//		}

		return sortedResultJson;
	}

	protected ArrayList<Point2D> parsePoints(String points) {
		ArrayList<Point2D> pts = new ArrayList<Point2D>();

		String[] splitString = points.split("\\|");
		for (int i = 0; i < splitString.length; i++) {
			String pointString = splitString[i];
			String[] pointSplitString = pointString.split(",");
			// System.out.println(pointString);
			if (pointSplitString.length == 2) {
				try {
					pts.add(new Point2D.Double(Double.parseDouble(pointSplitString[0]),
							Double.parseDouble(pointSplitString[1])));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}

		}
		return pts;
	}

	protected double[][] createSortedDistanceMatrix(ArrayList<Point2D> points, int minDistanceBetweenPoints) {
		int pointSize = points.size();
		double[][] distanceMatrix = new double[pointSize][pointSize];

		for (int i = 0; i < pointSize; i++) {
			for (int j = 0; j < pointSize; j++) {
				distanceMatrix[i][j] = -1.0;
			}
			distanceMatrix[i][i] = 0.0;
		}
		boolean[] cutList = new boolean[pointSize];
		int cutListLength = 0;

		for (int i = 0; i < cutList.length; i++) {
			cutList[i] = false;
		}

		for (int i = 0; i < pointSize; i++) {
			for (int j = 0; j < i; j++) {
				double distance = points.get(i).distance(points.get(j));
				distanceMatrix[i][j] = distance;
				distanceMatrix[j][i] = distance;
				if (distance <= minDistanceBetweenPoints) {
					cutList[i] = true;
				}

			}
		}
		for (int i = 0; i < cutList.length; i++) {
			if (cutList[i]) {
				cutListLength++;
			}
		}
		double[][] cutDistanceMatrix = new double[pointSize - cutListLength][pointSize - cutListLength];
		int i1 = 0;
		int j1 = 0;
		for (int i = 0; i < pointSize; i++) {
			if (!cutList[i]) {
				j1 = 0;
				for (int j = 0; j < pointSize; j++) {
					if (!cutList[j]) {
						cutDistanceMatrix[i1][j1] = distanceMatrix[i][j];
						j1++;
					}
				}
				i1++;
			}
		}

		for (int i = 0; i < (pointSize - cutListLength); i++) {
			for (int l = 0; l < (pointSize - cutListLength); l++) {
				for (int m = l; m > 0; m--) {
					if (cutDistanceMatrix[i][m] < cutDistanceMatrix[i][m - 1]) {
						double temp = cutDistanceMatrix[i][m];
						cutDistanceMatrix[i][m] = cutDistanceMatrix[i][m - 1];
						cutDistanceMatrix[i][m - 1] = temp;
					}
				}
			}

		}

		return cutDistanceMatrix;

	}

	protected double calcSamePointFactor(int databasePointSize, int featurePointSize, boolean nullFlag) {
		double samePointFactor = 1.0;
		if (nullFlag) {
			if (Math.abs(databasePointSize - featurePointSize) == 1) {
				samePointFactor = 0.75;
			} else if (Math.abs(databasePointSize - featurePointSize) == 2) {
				samePointFactor = 0.5;

			} else if (Math.abs(databasePointSize - featurePointSize) >= (featurePointSize * 0.5)) {
				samePointFactor = 0.0;
			} else {
				samePointFactor -= (Math.abs(databasePointSize - featurePointSize) / ((featurePointSize * 0.5)));
			}
		} else {
			double factor = 1.0;
			if (featurePointSize > databasePointSize) {
				factor = (databasePointSize / (double) featurePointSize);

			} else {
				factor = (featurePointSize / (double) databasePointSize);

			}
			samePointFactor -= (1.0 - factor);
		}

		return samePointFactor;
	}

	protected static void printArray(double matrix[][]) {
		for (double[] row : matrix)
			System.out.println(Arrays.toString(row));
	}

	protected double matchNearestPoints(ArrayList<Point2D> databasePoints, ArrayList<Point2D> featurePoints) {

		databasePoints = this.nearPointFilter(databasePoints, 20);
		featurePoints = this.nearPointFilter(featurePoints, 20);

		int databasePointSize = databasePoints.size();
		int featurePointSize = featurePoints.size();
		// System.out.println("database: " + databasePointSize + " | feature:" +
		// featurePointSize);

		int pointdiff = Math.abs(databasePointSize - featurePointSize);
		int compareIndex = Math.min(featurePointSize, databasePointSize);

		double squaredErrorSum = 0.0;

		for (int i = 0; i < compareIndex; i++) {
			int minErrorIndex = -1;
			double minError = Double.MAX_VALUE;

			for (int j = 0; j < databasePointSize - i; j++) {
				double error = featurePoints.get(i).distance(databasePoints.get(j));
				if (error < minError) {
					minError = error;
					minErrorIndex = j;
				}
			}
			databasePoints.remove(minErrorIndex);
			squaredErrorSum += minError * minError;

		}
		squaredErrorSum /= (double) compareIndex;
		double samePointFactor = this.calcSamePointFactor(databasePointSize, featurePointSize, false);
		// System.out.println("sqared Error: " + squaredErrorSum + " |
		// SamePointFactor: " + samePointFactor + " |D: "
		// + databasePointSize + " | D: " + featurePointSize);
		//
		// System.out.println(samePointFactor);

		return Math.max(0,
				Math.round((100 - 100 * (squaredErrorSum / (5000 * compareIndex))) * samePointFactor * 100) / 100.0);
	}

	protected ArrayList<Point2D> nearPointFilter(ArrayList<Point2D> points, int nearestPointDistance) {
		int initSize = points.size();
		int removeCounter = 0;
		for (int i = 0; i < initSize - removeCounter; i++) {
			for (int j = 0; j < initSize - removeCounter; j++) {
				if (i >= initSize - removeCounter) {
					continue;
				}
				if (i != j) {
					// System.out.println(initSize + " |i: " + i + " | j: " + j
					// + " | " + removeCounter );
					if (points.get(i).distance(points.get(j)) < nearestPointDistance) {
						points.remove(i);
						removeCounter++;
						continue;
					}
				}
			}
		}
		return points;
	}

	protected double matchPoints(ArrayList<Point2D> databasePoints, ArrayList<Point2D> featurePoints) {

		double[][] distanceMatrix = createSortedDistanceMatrix(databasePoints, this.minDistanceBetweenPoints);
		double[][] featureDistanceMatrix = createSortedDistanceMatrix(featurePoints, this.minDistanceBetweenPoints);
		int pointCounter = 0;
		int databasePointSize = distanceMatrix[0].length;
		int featurePointSize = featureDistanceMatrix[0].length;

		double overallPointError = 0;
		double[][] onePointSquaredError = new double[featurePointSize][2];

		for (int i = 0; i < onePointSquaredError.length; i++) {
			onePointSquaredError[i][0] = Double.MAX_VALUE;
			onePointSquaredError[i][1] = 0;
		}

		List<Integer> compareIndex = new ArrayList<Integer>();
		for (int h = 0; h < databasePointSize; h++) {
			compareIndex.add(h);
		}
		for (int i = 0; i < featurePointSize; i++) {
			int saveH = -1;
			int counter = 0;
			if (!compareIndex.isEmpty()) {
				for (Integer h : compareIndex) {
					int jMax = Math.min(3, Math.min(featureDistanceMatrix[i].length, distanceMatrix[h].length));

					for (int j = 0; j < jMax; j++) {
						double pow = Math.pow((featureDistanceMatrix[i][j] - distanceMatrix[h][j]), 2);
						// System.out.print("j=" + j +": " + pow + " | ");
						onePointSquaredError[i][1] += pow;
					}
					if (onePointSquaredError[i][1] < onePointSquaredError[i][0]) {
						onePointSquaredError[i][0] = onePointSquaredError[i][1];
						saveH = counter;
					}
					onePointSquaredError[i][1] = 0;
					counter++;
				}
				if (saveH != -1) {
					compareIndex.remove(saveH);
					overallPointError += onePointSquaredError[i][0];
					pointCounter++;
				}
			}
		}

		double samePointFactor = this.calcSamePointFactor(databasePointSize, featurePointSize, true);

		// System.out.println((100 - 100 * ( overallPointError / (500000 *
		// pointCounter))) + " | " + samePointFactor);
		return Math.max(0,
				Math.round((100 - 100 * (overallPointError / (500000 * pointCounter))) * samePointFactor * 100)
						/ 100.0);
	}

	protected ArrayList<Point2D> rotateFeaturePoints(ArrayList<Point2D> featurePoints, int rotDeg) {
		ArrayList<Point2D> rotFeaturePoints = new ArrayList<Point2D>();

		GeometryFactory geometryFactory = new GeometryFactory();
		if (featurePoints.size() == 1) {
			rotFeaturePoints.add(featurePoints.get(0));
		} else {
			Coordinate[] coords = new Coordinate[featurePoints.size()];
			int i = 0;
			for (Point2D point : featurePoints) {
				coords[i] = new Coordinate(point.getX(), point.getY());
				i++;
			}
			LineString lineString = geometryFactory.createLineString(coords);

			AffineTransformation transformation = new AffineTransformation();
			transformation.rotate(Math.toRadians((double) rotDeg));

			LineString rotatedLineString = (LineString) transformation.transform(lineString);
			rotFeaturePoints = this.scaletranslatePoints(rotatedLineString);

		}
		return rotFeaturePoints;
	}

	private ArrayList<Point2D> scaletranslatePoints(LineString ls) {
		double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE, maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;
		ArrayList<Point2D> featurePoints = new ArrayList<Point2D>();

		for (int i = 0; i < ls.getNumPoints(); i++) {
			Point2D point = new Point2D.Double(ls.getCoordinateN(i).x, ls.getCoordinateN(i).y);
			featurePoints.add(point);
			if (point.getX() < minX) {
				minX = point.getX();
			}
			if (point.getX() > maxX) {
				maxX = point.getX();
			}
			if (point.getY() < minY) {
				minY = point.getY();
			}
			if (point.getY() > maxY) {
				maxY = point.getY();
			}
		}

		double xDelta = maxX - minX;
		double yDelta = maxY - minY;
		double scaleFactor;

		int xOffset = 0, yOffset = 0;

		if (xDelta > yDelta) {
			if (xDelta == 0) {
				scaleFactor = 1;
			} else {
				scaleFactor = WINDOW_SIZE / xDelta;

			}
			yOffset = (int) (WINDOW_SIZE - (yDelta * scaleFactor)) / 2;

		} else {
			if (yDelta == 0) {
				scaleFactor = 1;
			} else {
				scaleFactor = WINDOW_SIZE / yDelta;

			}
			xOffset = (int) (WINDOW_SIZE - (xDelta * scaleFactor)) / 2;

		}

		for (Point2D p : featurePoints) {
			double x = (p.getX() - minX) * scaleFactor + xOffset;
			double y = (p.getY() - minY) * scaleFactor + yOffset;
			p.setLocation(x, y);
		}
		return featurePoints;
	}

}
